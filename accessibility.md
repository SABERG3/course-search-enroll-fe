# Accessibility Change Log

## Version 1.0.45

## Fixed

- Incorporate LiveAnnouncer into Collapse/Expand all sections buttons

  - Add LiveAnnouncer functionality to announce the UI change followed by closing/opening all accordion panels
  - Changed the text on buttons to be consistent across the app, since they provide the same functionality functionality (!446)

- DARS form elements - accessibility fixes

  - Credits step: add aria-labelledby to select dropdown with credits
  - Credits step: Fix aria-label to say credits instead of cr
  - Increase the font-size of form labels to 14px
  - Add attr.aria-label to the next/prev buttons on table pagination so that the screen reader will announce the change. It will now say 'Switched to page ' + pageEvent.pageIndex + ' of ' + title + ' table.'
  - Add aria-live="polite" to containers with cdkDropList attribute - this provides a slightly better experience as screen reader announces the container (List of courses in {{ termCode | getTermDescription }} term) below the dragged course
  - Increase the font of the caption text above audit tables to 16px (!444)

- Main DARS view- accessibility fixes

  - Fix heading levels to include h1, and h2 on main DARS view
  - Apply initially specified design to the Degree AUdit and What-if audit tables
  - Change table headings <th>s from 12px to 14px
  - Move School/College column to the first column in tables
  - Move View button to the left side of the Download button, and give this column a heading of Actions. This change solved the issue of having an empty <th> cell, which should never be without a data
  - Add scope="col attribute to each <th>
  - Add tooltip to Download icon
  - Increased font-size of form field labels to 16px (!440)

- Audit details view - accessibility fixes
  - Fix mixed heading levels, and include h1, h2,and h3 on Audit details view
  - Edit headings on the first table to be more descriptive ex. Degree instead of Deg, and increase the font-size from 12px to 14px
  - Add aria-labels to Close/open sections buttons
  - Add a "Degree Audit Requirements" (h3) heading above close/open buttons
  - Add scope="col" to all table headings
  - Add tooltips to all status icons (!433)

## Version 1.0.44

## Fixed

- DARS Initial Accessibility testing
  - Add visually hidden text to status icons next the expansion panels titles so that they communicate some information to the user
  - Review buttons and links: add descriptive aria-labels wherever needed (!417)

## Version 1.0.43

### Fixed

- Review aria-label for course keys, expand/hide all buttons, and 3 dot menu

  - Review aria-labels for expand all / collapse all buttons
  - Add course name to 3 dot menu aria-label so the screen reader will read "Open menu options for {{ course | courseDescription }}"
  - Remove list and listitem roles from course keys, which should be used for decorative elements only. Screen reader omitted elements with these roles (!389)

- Scroll to bottom after adding new academic year (!362)

- Fix skip to links

  - Remove visually hidden H1
  - Add anchor to Skip to main content link, to scroll to Degree Plans dropdown (!365)

- Review Skip to... links

  - Add anchor to Skip to utility menu link, to scroll to right hand sidenav (!361)

- Remove tabindex attribute from non-interactive elements
  - These changes follow guidance from Sandi and from the general DOM specification that the tabindex attribute (along with Angular's isFocusable and isTabbable attributes) should only be included on elements that can be interacted with via mouse clicks or keyboard input.
  - A number of elements in the degree planner view were non-interactive by design but still were in tabbable. This commit removes tabbing from those non-interactive elements. (!360)

## Version 1.0.19

### Fixed

- Elements grouping and aria-label revision

  - Remove focus style from non-reactive elements
  - Group term header elements (H3, total credits number, 3-dot menu)
  - Group term courses containers, saved for later, course keys, utility menu items
  - Add aria-labelledby to a star icon and check if plan on the degree plans list is primary, to announce it
  - Add number of courses to aria-label on tabs in terms (in progress, planned etc.) (!356)

- Review "Skip to main content" button functionality

  - Add missing id to h1, to fix "Skip to main content" link
  - Create additional link to "Skip to utility menu"
  - Change Delete Note from h2 to h3 for semantic markup
  - Change labels for Edit note and Add note to include term's name
  - Update the text on note dialog to - This action cannot be undone (!303)

- Remove redundant tooltips and adjust position of remaining ones to match mobile view

  - Remove tooltips from course keys icons on sidenav
  - Remove tooltips from menu buttons on sidenav
  - Adjust positions of remaining tooltips to match how they are displayed on mobile (!277)

- Fix color text for hint and label for invalid subject on search form

  - Change color (from #0479a8 blue to #ff4e4e red) of the hint and label of invalid Subject field on Search form, as well as change position of Subject hint to be left aligned (!273)

- Drag and drop accessibility improvement

  - Use the cdkDragStarted property to check for the click and drag event
  - Set an assistive text to be read by the screen reader (text is invisible on UI)
  - Not applied to Saved for Later or courses in search yet (!247)

- Implement "Skip to main content" functionality

  - Add "Skip to main content" button - keyboard and screen reader users will see a button on the top-left corner which offers skipping the header and main navigation entirely and takes users to the main Degree Planner content
  - Change icon for 'Course is not offered' to strikethrough
  - Increase the width of Degree Plans dropdown to 300px (!245)

- Give all course-items a role of button, and appropriate label (!242)

- Include unique h1 on DP view visible for screen readers only

  - Add Degree Planner h1 right after main navigation - consistent location with Course Search app
  - Add sr-only class and assign to it styles recommended by WebAIM (https://webaim.org/techniques/css/invisiblecontent/) to visually hide content but still be read by a screen reader
  - Screen readers will still highlight the h1 and it will be slightly visible when tabbing through the content (!241)

- Aria implementation

  - Change academic year panel title to h2 for headings hierarchy
  - Change term description title within term container from h2 to h3
  - Add missing aria-labels
  - Change the size of hint messages to 14px
  - Add aria-describedby to saved for later, planned, and enrolled courses containers
  - Replace #ff8000 orange warning color on inputs with #0479a8 blue (!234)

- Cross browser accessibility fix for outline style. Continue implementing aria and roles

  - Apply outline style to elements that were missing focus across the browsers
  - Add missing roles to applicable elements (main, navigation)
  - Improve/add missing aria-labels (!232)

- Add focus state and style to tabbable elements (!216)

- Alt-text revisions and aria-labels revision on icons, buttons, links

  - Add role="img" to svgs
  - Refactor icon’s alt texts to not include the word 'icon'
  - Add aria-label to buttons and links containing only icons (!212)

- Adjust icons font size and yellow color used on the app to a darker shade to fix low color contrast (!209)

  - Changed all icon font size to 24px
  - Changed yellow color used on the map to a darker shade

- Font size adjustments:

  - Increase academic year title font to 18px (!184)

- Initial accessibility fixes
  - Change "Add degree plan" input field placeholder from "i.e. Psychology" to "Plan Name"
  - Add tooltips and alt text to all icons used within the app
  - Add A11y module to the project
  - Add cdkFocusInitial where needed to specify which element to focus on first in the region ex: after opening a dialog, the focus lands on the first input field
  - Include word 'year' in academic years titles ex: Past year: 2017-2018; Current year: 2019 - 2020 etc. Instead of just 2019 - 2020 (!166)
