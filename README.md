# Course Search & Enroll - Front End

#### First time run `npm install -g @angular/cli` then `npm install`

#### Next run `npm start` to start server

## Required Reading!

New to Angular? [Angular tutorial](https://angular.io/tutorial)

[Official Angular Style Guide](https://angular.io/guide/styleguide) Go to source for all things Angular

New to Angular Material? [Angular Material](https://material.angular.io/compents/categories)

[Angular Material Guides](https://material.angular.io/guides) Set of nice howtos

## Before you start

Consider a lightweight IDE for developing [Visual Studio Code](https://code.visualstudio.com/)
works great with typescript ecosystem.

## Development server

Run `ng serve --proxy-config proxy.json` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Running TS Linter

Run `ng lint` to excute typescript linting. `ng lint --fix` will attempt to fix any errors

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Accessibilty Change Log

[Accessibility Change Log](accessibility.md)

## Dependancies

[Angular](http://angular.io)

[Angular CLI](https://github.com/angular/angular-cli)

[Angular Material 2](https://material.angular.io)

[UW Style](https://git.doit.wisc.edu/uw-madison-digital-strategy/uw-style)

[Material Design Icons](https://material.io/icons/) If searching for an icon look here: [Material Icons](https://material.io/icons/) These icons are added by a `<link>` tag in the `index.html` file.

## Grid System

UW-Style comes with a basic grid system borrowed from Foundation 6. See their documentation here: [Foundation Grid](https://foundation.zurb.com/sites/docs/flex-grid.html). **Note:** that the `.column` and `.row` classes for Foundation have been renamed to `.uw-row` and `.uw-col` and you will need to use the `uw-flex-column` mixin to build your grid widths. You cannot use `small-6, medium-8` etc. There are various examples in the [UW Style](https://git.doit.wisc.edu/uw-madison-digital-strategy/uw-style) repository.

If you are doing a more complicated layout it is recommended that you use Angular's official layout engine [Angular Flex Layout](https://github.com/angular/flex-layout). This layout is not CSS based and integrates deeper with Angular. Follow their documentation and `npm install` and add this to your project. To learn more about the grid system see their docs here: [Angular Flex Layout Wiki](https://github.com/angular/flex-layout/wiki)

### Removing UW-Style sass imports

If you want to use a different grid system or find yourself wanting to remove a portion of the css, comment out any `@imports` located here: `/assets/uwstyle/assets/scss/uw_style.scss`

## Basic project structure

<pre>
|-- app
|   |-- app.component.html
|   |-- app.component.scss
|   |-- app.component.spec.ts					# All tests end with .spec.ts
|   |-- app.component.ts
|   |-- app.module.ts  							# Parent app module
|   |-- app.routing.module.ts				    # Primary place to add new routes
|   |-- core
|   |   |-- config.service.ts 					# Used for config vars
|   |   |-- core.module.ts
|   |   |-- data.service.spec.ts				# Designed to mock data
|   |   |-- data.service.ts						# One catch all service for data transmission
|   |   |-- models 								# Placeholder for user created models/interfaces
|   |   |-- module-import-check.ts 				# Prevents the core module from loading twice.
|   |   `-- navigation						    # Shared navigation component
|   |       |-- navigation.component.html
|   |       |-- navigation.component.scss
|   |       |-- navigation.component.spec.ts
|   |       `-- navigation.component.ts
|   |-- home 									# Starter component using the router
|   |   |-- home.component.html
|   |   |-- home.component.scss
|   |   |-- home.component.spec.ts
|   |   `-- home.component.ts
|   `-- shared									# App wide shared directory. All modules could have a shared Directory too
|       `-- shared.module.ts  					# Only add Material components here!
|-- assets
|   |-- material-theme.scss  					# Custom Material theme
|   `-- uwstyle				  					# Copy of UWStyle
|       |-- dist
|       |-- fonts
|       `-- images
|-- index.html									# html entry point gets app.component
|-- main.ts
|-- polyfills.ts								# Enables compatibility for older browsers
|-- styles.css									# Could be used for global styles/imports
|-- test.ts										# Required by karma
|-- tsconfig.app.json
`-- tsconfig.spec.json

../angular.json 						    	# All project config for the angular cli
../tslint.json 									# All project lint settings ex: "ng lint"
../e2e/											# All project end to end tests ex: "ng e2e"
../tsconfig.json 								# Primary typescript config
</pre>
