import { Action } from '@ngrx/store';
import { UserPreferences } from '@app/core/models/user-preferences';

export enum CoreActionTypes {
  Initialize = '[Core] Initialize',

  PopulateUserPreferences = '[User Preferences] Populate',
  UpdateUserPreferences = '[User Preferences] Update',

  DismissAlert = '[UI] Dismiss Alert',
}

export class Initialize implements Action {
  public readonly type = CoreActionTypes.Initialize;
}

export class InitializeUserPreferences implements Action {
  public readonly type = CoreActionTypes.PopulateUserPreferences;
  constructor(public payload: { preferences: UserPreferences }) {}
}

export class UpdateUserPreferences implements Action {
  public readonly type = CoreActionTypes.UpdateUserPreferences;
  constructor(public payload: Partial<UserPreferences>) {}
}

export class DismissAlert implements Action {
  public readonly type = CoreActionTypes.DismissAlert;
  constructor(public payload: { key: string }) {}
}
