import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import * as selectors from '@app/degree-planner/store/selectors';
import { MatDialog, MatSnackBar } from '@angular/material';
import { PromptDialogComponent } from '@app/shared/dialogs/prompt-dialog/prompt-dialog.component';
import { CreatePlan } from '@app/degree-planner/store/actions/plan.actions';
import { GlobalState } from '../state';
import {
  OpenSidenav,
  CloseSidenav,
  AddAcademicYear,
} from '@app/degree-planner/store/actions/ui.actions';
import { MediaMatcher } from '@angular/cdk/layout';
import { environment } from './../../../environments/environment';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

const isntUndefined = <T>(anything: T | undefined): anything is T => {
  return anything !== undefined;
};

@Component({
  selector: 'cse-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public activeView: 'Degree Planner' | 'DARS';
  public activeRoadmapId$: Observable<number>;
  public mobileView: MediaQueryList;
  public isSidenavOpen$: Observable<boolean>;
  public useNewDARSView?: boolean;
  constructor(
    private store: Store<GlobalState>,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    public mediaMatcher: MediaMatcher,
    private location: Location,
    private router: Router,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
    this.useNewDARSView = environment['useNewDARSView'] === true;
  }

  ngOnInit() {
    this.router.events.subscribe(() => {
      if (this.location.path() === '') {
        this.activeView = 'Degree Planner';
      } else if (this.location.path() === '/dars') {
        this.activeView = 'DARS';
      }
    });

    this.activeRoadmapId$ = this.store.pipe(
      select(selectors.selectVisibleDegreePlan),
      filter(isntUndefined),
      map(plan => plan.roadmapId),
    );

    this.isSidenavOpen$ = this.store.pipe(select(selectors.isSidenavOpen)).pipe(
      map(isSidenavOpen => {
        if (isSidenavOpen === 'defer') {
          return !this.mobileView.matches;
        }

        return isSidenavOpen;
      }),
    );
  }

  public openSidenav() {
    this.store.dispatch(new OpenSidenav());
  }

  public closeSidenav() {
    this.store.dispatch(new CloseSidenav());
  }

  public addPlan() {
    this.dialog
      .open(PromptDialogComponent, {
        data: {
          title: 'Add degree plan',
          confirmText: 'Save',
          inputName: 'i.e. Psychology',
        },
      })
      .afterClosed()
      .subscribe((result: { confirmed: boolean; value: string }) => {
        const { confirmed, value } = result;
        if (confirmed) {
          const action = new CreatePlan({ name: value, primary: false });
          this.store.dispatch(action);
        }
      });
  }

  public addYear() {
    this.store.dispatch(new AddAcademicYear());
    this.snackBar.open('New academic year has been created');
  }
}
