export interface CourseBase {
  id: number | null;
  courseId: string;
  termCode: string | null;
  topicId: number;
  title: string;
  subjectCode: string;
  catalogNumber: string;
  credits?: number;
  creditMin?: number;
  creditMax?: number;
  grade?: any;
  classNumber: string | null;
  courseOrder: number;
  honors: string;
  waitlist: string;
  relatedClassNumber1?: any;
  relatedClassNumber2?: any;
  classPermissionNumber?: any;
  sessionCode?: any;
  validationResults: any[];
  enrollmentResults: any[];
  pendingEnrollments: any[];
  details?: any;
  classMeetings?: any;
  enrollmentOptions?: any;
  packageEnrollmentStatus?: any;
  creditRange?: any;
  studentEnrollmentStatus:
    | null
    | 'Enrolled'
    | 'Waitlisted'
    | 'cart'
    | 'NOTOFFERED'
    | 'DOESNOTEXIST';
}

export interface Course extends CourseBase {
  termCode: string;
}

export interface SubjectCodesTo<T> {
  readonly [subjectCode: string]: T;
}

export interface SubjectDescription {
  readonly short: string;
  readonly long: string;
}
