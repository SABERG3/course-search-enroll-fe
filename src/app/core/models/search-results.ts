import { Subject } from '@app/core/models/course-details';

export interface SearchResult {
  termCode: string;
  courseId: string;
  subject: Subject;
}

export interface SearchResults {
  found: number;
  hits: SearchResult[];
  message: null;
  success: boolean;
}
