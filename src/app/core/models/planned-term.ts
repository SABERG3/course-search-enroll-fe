import { Course } from '@app/core/models/course';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';

export type PlannedTermNote =
  | { isLoaded: true; text: string; id: number }
  | { isLoaded: false; text: string };

export interface PlannedTerm {
  roadmapId: number;
  termCode: TermCode;
  note?: PlannedTermNote;
  plannedCourses: ReadonlyArray<Course>;
  enrolledCourses: ReadonlyArray<Course>;
}
