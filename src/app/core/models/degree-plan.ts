export interface DegreePlan {
  roadmapId: number;
  pvi: string;
  name: string;
  primary: boolean;
}
