import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { environment } from './../../environments/environment';

import { SharedModule } from '../shared/shared.module';
import { NavigationComponent } from './navigation/navigation.component';
import { throwIfAlreadyLoaded } from './module-import-check';
import { EffectsModule } from '@ngrx/effects';
import { CoreEffects } from './effects';
import { StoreModule } from '@ngrx/store';
import { preferencesReducer } from './reducer';

@NgModule({
  imports: [
    CommonModule, // we use *ngFor
    RouterModule, // we use router-outlet and routerLink
    SharedModule,
    StoreModule.forRoot({ preferences: preferencesReducer }),
    EffectsModule.forFeature([CoreEffects]),
  ],
  exports: [NavigationComponent],
  declarations: [NavigationComponent],
  providers: [
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: { duration: environment.snackbarDuration },
    },
  ],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
