import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap, withLatestFrom, flatMap, map } from 'rxjs/operators';
import { GlobalState } from '@app/core/state';
import { Store } from '@ngrx/store';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import * as actions from './actions';
import { CoreActionTypes } from './actions';
import { UserPreferences } from './models/user-preferences';

@Injectable()
export class CoreEffects {
  constructor(
    private actions$: Actions,
    private api: DegreePlannerApiService,
    private store$: Store<GlobalState>,
  ) {}

  @Effect()
  initializeUserPreferences$ = this.actions$.pipe(
    ofType(CoreActionTypes.Initialize),
    flatMap(() => this.api.getUserPreferences()),
    map(preferences => new actions.InitializeUserPreferences({ preferences })),
  );

  @Effect({ dispatch: false })
  updateUserPreferences$ = this.actions$.pipe(
    ofType<actions.UpdateUserPreferences>(
      CoreActionTypes.UpdateUserPreferences,
    ),
    withLatestFrom(this.store$),
    tap(([update, state]) => {
      const prefs: UserPreferences = { ...state.preferences };

      // Remove properties who's value is undefined
      Object.entries(update.payload).forEach(([key, value]) => {
        if (value === undefined && prefs[key] !== undefined) {
          delete prefs[key];
        } else {
          prefs[key] = value;
        }
      });

      this.api.updateUserPreferences(prefs).toPromise();
      // Must .toPromise() the API call for it to run
    }),
  );
}
