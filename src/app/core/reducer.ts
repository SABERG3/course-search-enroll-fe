import { CoreActionTypes, UpdateUserPreferences } from './actions';
import * as actions from './actions';
import { INITIAL_PREFERENCES_STATE } from './state';
import { UserPreferences } from './models/user-preferences';

type SupportedActions =
  | actions.InitializeUserPreferences
  | actions.UpdateUserPreferences;

export function preferencesReducer(
  state = INITIAL_PREFERENCES_STATE,
  action: SupportedActions,
): UserPreferences {
  switch (action.type) {
    case CoreActionTypes.PopulateUserPreferences: {
      return action.payload.preferences;
    }
    case CoreActionTypes.UpdateUserPreferences: {
      const update: UserPreferences = { ...state };

      // Remove properties who's value is undefined
      Object.entries(action.payload).forEach(([key, value]) => {
        if (value === undefined && update[key] !== undefined) {
          delete update[key];
        } else {
          update[key] = value;
        }
      });

      return update;
    }
    default:
      return state;
  }
}
