import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { environment } from './../../../environments/environment';
import { FeedbackDialogComponent } from '@app/degree-planner/dialogs/feedback-dialog/feedback-dialog.component';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'cse-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit {
  public activeView: 'Degree Planner' | 'DARS';
  public useNewDARSView?: boolean;

  constructor(
    public dialog: MatDialog,
    private location: Location,
    private router: Router,
  ) {
    this.useNewDARSView = environment['useNewDARSView'] === true;
  }
  ngOnInit() {
    this.router.events.subscribe(() => {
      if (this.location.path() === '') {
        this.activeView = 'Degree Planner';
      } else if (this.location.path() === '/dars') {
        this.activeView = 'DARS';
      }
    });
  }
  public onFeedbackClick() {
    this.dialog.open(FeedbackDialogComponent, {
      closeOnNavigation: true,
    });
  }
}
