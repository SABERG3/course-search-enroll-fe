import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Component, Input, OnInit } from '@angular/core';
import { GlobalState } from '@app/core/state';
import * as selectors from '@app/degree-planner/store/selectors';
import {
  ExpandAcademicYear,
  CollapseAcademicYear,
} from '@app/degree-planner/store/actions/ui.actions';
import { YearCode } from '../shared/term-codes/yearcode';

@Component({
  selector: 'cse-year-container',
  templateUrl: './year-container.component.html',
  styleUrls: ['./year-container.component.scss'],
})
export class YearContainerComponent implements OnInit {
  @Input() yearCode: YearCode;

  public isExpanded$: Observable<boolean>;

  constructor(private store: Store<GlobalState>) {}

  public ngOnInit(): void {
    this.isExpanded$ = this.store.pipe(
      select(selectors.selectYearExpandedState, { yearCode: this.yearCode }),
    );
  }

  public expandYear() {
    this.store.dispatch(new ExpandAcademicYear({ yearCode: this.yearCode }));
  }

  public collapseYear() {
    this.store.dispatch(new CollapseAcademicYear({ yearCode: this.yearCode }));
  }
}
