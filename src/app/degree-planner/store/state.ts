import { YearMapping } from '@app/core/models/year';
import { DegreePlan } from '@app/core/models/degree-plan';
import { SavedForLaterCourse } from '@app/core/models/saved-for-later-course';
import { SubjectCodesTo, SubjectDescription } from '@app/core/models/course';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';
import { Alert } from '@app/core/models/alert';

export interface DegreePlannerState {
  visibleDegreePlan: DegreePlan | undefined;
  visibleYears: YearMapping;
  savedForLaterCourses: ReadonlyArray<SavedForLaterCourse>;
  allDegreePlans: ReadonlyArray<DegreePlan>;
  subjectDescriptions: SubjectCodesTo<SubjectDescription>;
  search: { visible: boolean; selectedTerm?: TermCode };
  isLoadingPlan: boolean;
  isSidenavOpen: 'defer' | boolean;
  alerts: Alert[];
  showGrades: boolean;
}

export const INITIAL_DEGREE_PLANNER_STATE: DegreePlannerState = {
  visibleDegreePlan: undefined,
  visibleYears: {},
  savedForLaterCourses: [],
  allDegreePlans: [],
  subjectDescriptions: {},
  search: { visible: false },
  isLoadingPlan: true,
  isSidenavOpen: 'defer',
  alerts: [],
  showGrades: true,
};
