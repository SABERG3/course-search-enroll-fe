import { Action } from '@ngrx/store';
import { YearCode } from '@app/degree-planner/shared/term-codes/yearcode';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';

export enum UIActionTypes {
  ToggleAcademicYear = '[UI] Toggle Academic Year',
  ExpandAcademicYear = '[UI] Expand Academic Year',
  CollapseAcademicYear = '[UI] Collapse Academic Year',

  OpenCourseSearch = '[UI] Open Course Search',
  CloseCourseSearch = '[UI] Close Course Search',
  ToggleCourseSearch = '[UI] Toggle Course Search',

  UpdateSearchTermCode = '[UI] Change Search Term Code',

  OpenSidenav = '[UI] Open Sidenav',
  CloseSidenav = '[UI] Close Sidenav',

  UpdateUserPreferences = '[UI] Update User Preferences',

  AddAcademicYear = '[UI} Add Academic Year',
}

export class ToggleAcademicYear implements Action {
  public readonly type = UIActionTypes.ToggleAcademicYear;
  constructor(public payload: { yearCode: YearCode }) {}
}

export class ExpandAcademicYear implements Action {
  public readonly type = UIActionTypes.ExpandAcademicYear;
  constructor(public payload?: { yearCode: YearCode }) {}
}

export class CollapseAcademicYear implements Action {
  public readonly type = UIActionTypes.CollapseAcademicYear;
  constructor(public payload?: { yearCode: YearCode }) {}
}

export class OpenCourseSearch implements Action {
  public readonly type = UIActionTypes.OpenCourseSearch;
  constructor(public termCode?: TermCode) {}
}

export class CloseCourseSearch implements Action {
  public readonly type = UIActionTypes.CloseCourseSearch;
  constructor() {}
}

export class ToggleCourseSearch implements Action {
  public readonly type = UIActionTypes.ToggleCourseSearch;
  constructor(public termCode?: TermCode) {}
}

export class UpdateSearchTermCode implements Action {
  public readonly type = UIActionTypes.UpdateSearchTermCode;
  constructor(public termCode: TermCode) {}
}

export class OpenSidenav implements Action {
  public readonly type = UIActionTypes.OpenSidenav;
}

export class CloseSidenav implements Action {
  public readonly type = UIActionTypes.CloseSidenav;
}

export class AddAcademicYear implements Action {
  public readonly type = UIActionTypes.AddAcademicYear;
}
