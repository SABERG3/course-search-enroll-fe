import { Action } from '@ngrx/store';
import { Note } from '@app/core/models/note';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';

export enum NoteActionTypes {
  WriteNote = '[Note] Write',
  WriteNoteSuccess = '[Note] Write (Success)',

  DeleteNote = '[Note] Delete',
  DeleteNoteSuccess = '[Note] Delete (Success)',

  NoteError = '[Note] Error',
}

export class WriteNote implements Action {
  public readonly type = NoteActionTypes.WriteNote;
  constructor(public payload: { termCode: TermCode; noteText: string }) {}
}

export class WriteNoteSuccess implements Action {
  public readonly type = NoteActionTypes.WriteNoteSuccess;
  constructor(public payload: { termCode: TermCode; updatedNote: Note }) {}
}

export class DeleteNote implements Action {
  public readonly type = NoteActionTypes.DeleteNote;
  constructor(public payload: { termCode: TermCode; noteId: number }) {}
}

export class DeleteNoteSuccess implements Action {
  public readonly type = NoteActionTypes.DeleteNoteSuccess;
  constructor(public payload: { termCode: TermCode }) {}
}

export class NoteError implements Action {
  public readonly type = NoteActionTypes.NoteError;
  constructor(
    public payload: { message: string; duration: number; error: any },
  ) {}
}
