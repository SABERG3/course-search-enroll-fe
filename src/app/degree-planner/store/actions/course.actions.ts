import { Action } from '@ngrx/store';
import { Course } from '@app/core/models/course';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';

export enum CourseActionTypes {
  AddCourse = '[Course] Add',
  AddCourseSuccess = '[Course] Add (Success)',

  RemoveCourse = '[Course] Remove',
  RemoveCourseSuccess = '[Course] Remove (Success)',

  MoveCourseInsideTerm = '[Course] Move Inside Term',
  MoveCourseBetweenTerms = '[Course] Move Between Terms',
  MoveCourseBetweenTermsSuccess = '[Course] Move Between Terms (Success)',

  MoveCourseInsideSFL = '[Course] Move Inside SFL',

  AddSaveForLater = '[Course] Add Save for Later',
  AddSaveForLaterSuccess = '[Course] Add Save for Later (Success)',

  RemoveSaveForLater = '[Course] Remove Save for Later',
  RemoveSaveForLaterSuccess = '[Course] Remove Save for Later (Success)',

  CourseError = '[Course] Error',
}

export class MoveCourseInsideTerm implements Action {
  public readonly type = CourseActionTypes.MoveCourseInsideTerm;
  constructor(
    public payload: { termCode: TermCode; recordId: number; newIndex: number },
  ) {}
}

export class MoveCourseInsideSFL implements Action {
  public readonly type = CourseActionTypes.MoveCourseInsideSFL;
  constructor(public payload: { courseId: string; newIndex: number }) {}
}

export class MoveCourseBetweenTerms implements Action {
  public readonly type = CourseActionTypes.MoveCourseBetweenTerms;
  constructor(
    public payload: {
      to: TermCode;
      from: TermCode;
      id: number;
      newIndex?: number;
      courseId: string;
      subjectCode: string;
    },
  ) {}
}

export class MoveCourseBetweenTermsSuccess implements Action {
  public readonly type = CourseActionTypes.MoveCourseBetweenTermsSuccess;
  constructor(
    public payload: {
      to: TermCode;
      from: TermCode;
      id: number;
      newIndex?: number;
      courseId: string;
      subjectCode: string;
    },
  ) {}
}

export class AddCourse implements Action {
  public readonly type = CourseActionTypes.AddCourse;
  constructor(
    public payload: {
      courseId: string;
      termCode: TermCode;
      subjectCode: string;
      title: string;
      catalogNumber: string;
      newIndex?: number;
    },
  ) {}
}

export class AddCourseSuccess implements Action {
  public readonly type = CourseActionTypes.AddCourseSuccess;
  constructor(
    public payload: { termCode: TermCode; course: Course; newIndex?: number },
  ) {}
}

export class RemoveCourse implements Action {
  public readonly type = CourseActionTypes.RemoveCourse;
  constructor(public payload: { fromTermCode: TermCode; recordId: number }) {}
}

export class RemoveCourseSuccess implements Action {
  public readonly type = CourseActionTypes.RemoveCourseSuccess;
  constructor(public payload: { fromTermCode: TermCode; recordId: number }) {}
}

export class AddSaveForLater implements Action {
  public readonly type = CourseActionTypes.AddSaveForLater;
  constructor(
    public payload: {
      subjectCode: string;
      courseId: string;
      title: string;
      catalogNumber: string;
      newIndex: number;
    },
  ) {}
}

export class AddSaveForLaterSuccess implements Action {
  public readonly type = CourseActionTypes.AddSaveForLaterSuccess;
  constructor(
    public payload: {
      subjectCode: string;
      courseId: string;
      title: string;
      catalogNumber: string;
      newIndex: number;
    },
  ) {}
}

export class RemoveSaveForLater implements Action {
  public readonly type = CourseActionTypes.RemoveSaveForLater;
  constructor(public payload: { subjectCode: string; courseId: string }) {}
}

export class RemoveSaveForLaterSuccess implements Action {
  public readonly type = CourseActionTypes.RemoveSaveForLaterSuccess;
  constructor(public payload: { subjectCode: string; courseId: string }) {}
}

export class CourseError implements Action {
  public readonly type = CourseActionTypes.CourseError;
  constructor(public payload: { message: string; error: any }) {}
}
