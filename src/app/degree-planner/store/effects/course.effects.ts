import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of, forkJoin } from 'rxjs';
import {
  tap,
  map,
  flatMap,
  withLatestFrom,
  filter,
  catchError,
} from 'rxjs/operators';
import { GlobalState } from '@app/core/state';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import * as selectors from '@app/degree-planner/store/selectors';
import {
  AddCourse,
  AddCourseSuccess,
  AddSaveForLater,
  AddSaveForLaterSuccess,
  CourseActionTypes,
  CourseError,
  MoveCourseBetweenTerms,
  MoveCourseBetweenTermsSuccess,
  RemoveCourse,
  RemoveCourseSuccess,
  RemoveSaveForLater,
  RemoveSaveForLaterSuccess,
} from '@app/degree-planner/store/actions/course.actions';
import { DegreePlan } from '@app/core/models/degree-plan';
import { Course, CourseBase } from '@app/core/models/course';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';
import { ConstantsService } from '@app/degree-planner/services/constants.service';

@Injectable()
export class CourseEffects {
  constructor(
    private actions$: Actions,
    private constants: ConstantsService,
    private api: DegreePlannerApiService,
    private store$: Store<GlobalState>,
    private snackBar: MatSnackBar,
  ) {}

  @Effect()
  MoveCourseBetweenTerms$ = this.actions$.pipe(
    ofType<MoveCourseBetweenTerms>(CourseActionTypes.MoveCourseBetweenTerms),

    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),
    filter<[MoveCourseBetweenTerms, DegreePlan]>(([_, degreePlan]) => {
      return degreePlan !== undefined;
    }),

    // Get term data for the degree plan specified by the roadmap ID.
    flatMap(([action, degreePlan]) => {
      const roadmapId = degreePlan.roadmapId;
      const {
        id: recordId,
        to: toTermCode,
        subjectCode,
        courseId,
      } = action.payload;

      const moveCourse = this.api.updateCourseTerm(
        roadmapId,
        recordId,
        toTermCode,
      );

      if (toTermCode.isActive() && degreePlan.primary) {
        /**
         * The `updateCourseTerm` API won't force cart validation which we want
         * if we're adding a course to the cart. Calling the `addCourseToCart`
         * API when moving a cart to an active term will trigger the cart
         * validation.
         */
        const validateCart = this.api.addCourseToCart(
          subjectCode,
          courseId,
          toTermCode,
        );
        return forkJoin(moveCourse, validateCart).pipe(map(() => action));
      } else {
        return moveCourse.pipe(map(() => action));
      }
    }),

    map(action => new MoveCourseBetweenTermsSuccess(action.payload)),

    tap(action => {
      const touchedTerm = action.payload.to.description;
      const message = `Course has been moved to ${touchedTerm}`;
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        new CourseError({
          message: 'Unable to move course',
          error,
        }),
      );
    }),
  );

  @Effect()
  AddCourse$ = this.actions$.pipe(
    ofType<AddCourse>(CourseActionTypes.AddCourse),

    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),

    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),

    flatMap(([action, visibleDegreePlan]) => {
      // TODO error handle the API calls
      const roadmapId = (visibleDegreePlan as DegreePlan).roadmapId;
      const { subjectCode, termCode, courseId, newIndex } = action.payload;

      const isPrimaryPlan = (visibleDegreePlan as DegreePlan).primary;
      const addCourse$ =
        termCode.isActive() && isPrimaryPlan
          ? this.api.addCourseToCart(subjectCode, courseId, termCode)
          : this.api.addCourse(roadmapId, subjectCode, courseId, termCode);

      const courseBaseToCourse$ = addCourse$.pipe(
        map<CourseBase, Course>(courseBase => ({
          ...courseBase,
          termCode: termCode.toString(),
        })),
      );

      const toSuccessAction$ = courseBaseToCourse$.pipe(
        map(course => new AddCourseSuccess({ termCode, course, newIndex })),
      );

      return toSuccessAction$;
    }),

    tap(state => {
      const touchedCourse = state.payload.course;
      const touchedTerm = TermCode.describe(touchedCourse.termCode);
      const subject = this.constants.subjectDescription(
        touchedCourse.subjectCode,
      ).short;
      const message = `${subject} ${touchedCourse.catalogNumber} has been added to ${touchedTerm}`;
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        new CourseError({
          message: 'Unable to add course',
          error,
        }),
      );
    }),
  );

  @Effect()
  RemoveCourse$ = this.actions$.pipe(
    ofType<RemoveCourse>(CourseActionTypes.RemoveCourse),

    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),

    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),

    flatMap(([action, visibleDegreePlan]) => {
      const roadmapId = (visibleDegreePlan as DegreePlan).roadmapId;
      const recordId = action.payload.recordId;
      const fromTermCode = action.payload.fromTermCode;

      const removeCourse$ = this.api.removeCourse(roadmapId, recordId);
      const toSuccessAction$ = removeCourse$.pipe(
        map(() => new RemoveCourseSuccess({ fromTermCode, recordId })),
      );

      return toSuccessAction$;
    }),

    catchError(error => {
      return of(
        new CourseError({
          message: 'Unable to remove course',
          error,
        }),
      );
    }),
  );

  @Effect()
  RemoveSavedForLater$ = this.actions$.pipe(
    ofType<RemoveSaveForLater>(CourseActionTypes.RemoveSaveForLater),

    flatMap(action => {
      const { subjectCode, courseId } = action.payload;
      return this.api
        .removeSavedForLater(subjectCode, courseId)
        .pipe(map(() => action));
    }),

    map(action => new RemoveSaveForLaterSuccess(action.payload)),

    catchError(error => {
      return of(
        new CourseError({
          message: 'Unable to remove saved course',
          error,
        }),
      );
    }),
  );

  @Effect()
  SaveForLater$ = this.actions$.pipe(
    ofType<AddSaveForLater>(CourseActionTypes.AddSaveForLater),

    flatMap(action => {
      const { subjectCode, courseId } = action.payload;
      return this.api
        .saveForLater(subjectCode, courseId)
        .pipe(map(() => action));
    }),

    map(action => new AddSaveForLaterSuccess(action.payload)),

    tap(() => {
      const message = 'Course has been saved for later';
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        new CourseError({
          message: 'Unable to save course for later',
          error,
        }),
      );
    }),
  );
}
