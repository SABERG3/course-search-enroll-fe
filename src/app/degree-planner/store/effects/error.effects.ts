import {
  PlanError,
  PlanActionTypes,
} from '@app/degree-planner/store/actions/plan.actions';
import {
  NoteError,
  NoteActionTypes,
} from '@app/degree-planner/store/actions/note.actions';
import {
  CourseError,
  CourseActionTypes,
} from '@app/degree-planner/store/actions/course.actions';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class ErrorEffects {
  constructor(private actions$: Actions, private snackBar: MatSnackBar) {}

  @Effect({ dispatch: false })
  error$ = this.actions$.pipe(
    ofType<PlanError | NoteError | CourseError>(
      PlanActionTypes.PlanError,
      NoteActionTypes.NoteError,
      CourseActionTypes.CourseError,
    ),

    tap(action => {
      const message = action.payload.message;
      const err = action.payload.error;
      this.snackBar.open(message);
    }),
  );
}
