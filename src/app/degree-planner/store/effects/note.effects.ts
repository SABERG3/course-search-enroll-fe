import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import {
  mergeMap,
  withLatestFrom,
  tap,
  map,
  filter,
  catchError,
} from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';
import { Year, YearMapping } from '@app/core/models/year';
import { DegreePlan } from '@app/core/models/degree-plan';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import {
  NoteActionTypes,
  WriteNote,
  WriteNoteSuccess,
  DeleteNote,
  DeleteNoteSuccess,
  NoteError,
} from '@app/degree-planner/store/actions/note.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { GlobalState } from '@app/core/state';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';

@Injectable()
export class NoteEffects {
  constructor(
    private actions$: Actions,
    private store$: Store<GlobalState>,
    private api: DegreePlannerApiService,
    private snackBar: MatSnackBar,
  ) {}

  @Effect()
  write$ = this.actions$.pipe(
    ofType<WriteNote>(NoteActionTypes.WriteNote),

    // Get the most recent Degree Planner state object from the store. This is
    // used to decide to fire either the `updateNote` API or `createNote` API.
    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),

    // Only handle WriteNote actions when a current plan ID is set.
    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),

    withLatestFrom(this.store$.select(selectors.selectAllVisibleYears)),

    // Using the action and State objects, determine whether to fire the
    // `updateNote` or `createNote` API. Both of these API calls return
    // an observable wrapper around the modified/created Note object.
    mergeMap(([[action, visibleDegreePlan], years]) => {
      const planId = (visibleDegreePlan as DegreePlan).roadmapId;
      const termCode = action.payload.termCode;
      const noteText = action.payload.noteText;
      const existingNote = getExistingNote(years, termCode);
      if (existingNote !== undefined && existingNote.isLoaded) {
        // Since the term DOES have a note, update the existing note
        const noteId = existingNote.id;
        return this.api
          .updateNote(planId, termCode, noteText, noteId)
          .pipe(
            map(updatedNote => new WriteNoteSuccess({ termCode, updatedNote })),
          );
      } else {
        // Since the term DOES NOT have a note, create a new note
        return this.api
          .createNote(planId, termCode, noteText)
          .pipe(
            map(updatedNote => new WriteNoteSuccess({ termCode, updatedNote })),
          );
      }
    }),

    tap(() => {
      const message = 'Note has been saved';
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        new NoteError({
          message: 'Unable to save note',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  delete$ = this.actions$.pipe(
    ofType<DeleteNote>(NoteActionTypes.DeleteNote),

    // Get the most recent Degree Planner state object.
    // This is used to lookup the Note ID.
    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),

    // Only handle DeleteNote actions when a current plan ID is set.
    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),

    // Using the action and State objects, fire the `deleteNote` API.
    mergeMap(([action, visibleDegreePlan]) => {
      const planId = (visibleDegreePlan as DegreePlan).roadmapId;
      const { termCode, noteId } = action.payload;
      return this.api.deleteNote(planId, noteId).pipe(map(() => termCode));
    }),

    // Dispatch an `DeleteNoteSuccess` action so that the
    // State object can be updated with the note removed.
    map(termCode => new DeleteNoteSuccess({ termCode })),

    tap(() => {
      const message = 'Note has been deleted';
      this.snackBar.open(message, undefined, {});
    }),

    catchError(error => {
      return of(
        new NoteError({
          message: 'Unable to remove note',
          duration: 2000,
          error,
        }),
      );
    }),
  );
}

const getExistingNote = (years: YearMapping, termCode: TermCode) => {
  const year: Year | undefined = years[termCode.yearCode.toString()];

  if (year) {
    return year[termCode.termName].note;
  } else {
    return undefined;
  }
};
