import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, forkJoin, of } from 'rxjs';
import {
  tap,
  map,
  mergeMap,
  flatMap,
  withLatestFrom,
  catchError,
  filter,
} from 'rxjs/operators';
import { GlobalState } from '@app/core/state';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import * as selectors from '@app/degree-planner/store/selectors';
import {
  InitialLoadSuccess,
  SwitchPlan,
  SwitchPlanSuccess,
  PlanActionTypes,
  PlanError,
  MakePlanPrimary,
  MakePlanPrimarySuccess,
  MakePlanPrimaryFailure,
  ChangePlanName,
  ChangePlanNameSuccess,
  ChangePlanNameFailure,
  CreatePlan,
  CreatePlanSuccess,
  DeletePlan,
  DeletePlanSuccess,
  ChangeGradeVisibility,
} from '@app/degree-planner/store/actions/plan.actions';
import { DegreePlan } from '@app/core/models/degree-plan';
import { PlannedTerm, PlannedTermNote } from '@app/core/models/planned-term';
import { INITIAL_DEGREE_PLANNER_STATE } from '@app/degree-planner/store/state';
import { YearMapping, MutableYearMapping } from '@app/core/models/year';
import { Note } from '@app/core/models/note';
import { CourseBase, Course } from '@app/core/models/course';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';
import { Alert, DisclaimerAlert } from '@app/core/models/alert';
import { UpdateUserPreferences } from '@app/core/actions';
import { TermCodeFactory } from '@app/degree-planner/services/termcode.factory';

@Injectable()
export class DegreePlanEffects {
  constructor(
    private actions$: Actions,
    private api: DegreePlannerApiService,
    private store$: Store<GlobalState>,
    private snackBar: MatSnackBar,
    private termCodeService: TermCodeFactory,
  ) {}

  @Effect()
  init$ = this.actions$.pipe(
    ofType(PlanActionTypes.InitialLoadRequest),
    // Load the list of degree plans and data used by all degree plans.
    flatMap(() => {
      return forkJoinWithKeys({
        allDegreePlans: this.api.getAllDegreePlans(),
        activeTermCodes: this.api.getActiveTermCodes(),
        userPreferences: this.api.getUserPreferences(),
      });
    }),

    flatMap(({ allDegreePlans, activeTermCodes, userPreferences }) => {
      if (this.termCodeService.isNotInitialized()) {
        this.termCodeService.setActiveTermCodes(activeTermCodes);
      }

      const savedForLaterCourses = this.api.getSavedForLaterCourses();
      const visibleDegreePlan = userPreferences.degreePlannerSelectedPlan
        ? pickDegreePlanById(
            userPreferences.degreePlannerSelectedPlan,
            allDegreePlans,
          )
        : pickPrimaryDegreePlan(allDegreePlans);
      const visibleYears = loadPlanYears(
        this.api,
        visibleDegreePlan.roadmapId,
        this.termCodeService,
      );

      const alerts: Alert[] = [];

      if (userPreferences.degreePlannerHasDismissedDisclaimer !== true) {
        alerts.push(
          new DisclaimerAlert(() => {
            this.store$.dispatch(
              new UpdateUserPreferences({
                degreePlannerHasDismissedDisclaimer: true,
              }),
            );
          }),
        );
      }

      const showGrades =
        userPreferences.degreePlannerGradesVisibility !== undefined
          ? userPreferences.degreePlannerGradesVisibility
          : true;

      return forkJoinWithKeys({
        showGrades: of(showGrades),
        visibleDegreePlan: of(visibleDegreePlan),
        visibleYears,
        savedForLaterCourses,
        allDegreePlans: of(allDegreePlans),
        alerts: of(alerts),
      }).pipe(
        map(payload => {
          return new InitialLoadSuccess({
            ...INITIAL_DEGREE_PLANNER_STATE,
            ...payload,
            isLoadingPlan: false,
          });
        }),
      );
    }),
    catchError(error => {
      return of(
        new PlanError({
          message: 'Error loading data. Please reload to try again.',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  switch$ = this.actions$.pipe(
    ofType<SwitchPlan>(PlanActionTypes.SwitchPlan),
    withLatestFrom(this.store$.select(selectors.selectAllDegreePlans)),
    flatMap(([action, allDegreePlans]) => {
      const visibleDegreePlan = allDegreePlans.find(plan => {
        return plan.roadmapId === action.payload.newVisibleRoadmapId;
      }) as DegreePlan;

      const visibleYears = loadPlanYears(
        this.api,
        visibleDegreePlan.roadmapId,
        this.termCodeService,
      );

      return forkJoinWithKeys({
        visibleDegreePlan: of(visibleDegreePlan),
        visibleYears,
      });
    }),
    mergeMap(payload => [
      new SwitchPlanSuccess(payload),
      new UpdateUserPreferences({
        degreePlannerSelectedPlan: payload.visibleDegreePlan.roadmapId,
      }),
    ]),
    tap(state => {
      if (state instanceof SwitchPlanSuccess) {
        const touchedPlan = state.payload.visibleDegreePlan.name;
        const message = `Switched to ${touchedPlan}`;
        this.snackBar.open(message, undefined, {});
      }
    }),
    catchError(error => {
      return of(
        new PlanError({
          message: 'Unable to switch plan',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  gradeVisibility$ = this.actions$.pipe(
    ofType<ChangeGradeVisibility>(PlanActionTypes.ChangeGradeVisibility),
    withLatestFrom(this.store$),
    map(
      ([change, state]) =>
        new UpdateUserPreferences({
          degreePlannerGradesVisibility: change.visibility,
        }),
    ),
    catchError(error => {
      return of(
        new PlanError({
          message: 'Unable to change grade visibility',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  MakePlanPrimary$ = this.actions$.pipe(
    ofType<MakePlanPrimary>(PlanActionTypes.MakePlanPrimary),
    withLatestFrom(this.store$.select(selectors.selectVisibleDegreePlan)),
    filter(([_, visibleDegreePlan]) => visibleDegreePlan !== undefined),
    // Get term data for the degree plan specified by the roadmap ID.
    flatMap(([_action, visibleDegreePlan]) => {
      const { roadmapId, name } = visibleDegreePlan as DegreePlan;
      return this.api.updatePlan(roadmapId, name, true);
    }),
    // // Wrap data in an Action for dispatch
    map(response => {
      if (response === 1) {
        return new MakePlanPrimarySuccess();
      } else {
        return new MakePlanPrimaryFailure();
      }
    }),
    tap(() => {
      const message = 'This plan has been set as the primary plan';
      this.snackBar.open(message, undefined, {});
    }),
    catchError(error => {
      return of(
        new PlanError({
          message: 'Unable to make this plan primary',
          duration: 2000,
          error,
        }),
      );
    }),
  );

  @Effect()
  ChangePlanName$ = this.actions$.pipe(
    ofType<ChangePlanName>(PlanActionTypes.ChangePlanName),
    withLatestFrom(this.store$.select(selectors.selectAllDegreePlans)),
    flatMap(([action, allDegreePlans]) => {
      const { roadmapId, newName } = action.payload;
      const oldDegreePlan = allDegreePlans.find(plan => {
        return plan.roadmapId === roadmapId;
      }) as DegreePlan;
      const oldName = oldDegreePlan.name;

      return this.api
        .updatePlan(roadmapId, newName, oldDegreePlan.primary)
        .pipe(
          map(() => {
            return new ChangePlanNameSuccess({ roadmapId, newName });
          }),
          tap(() => {
            const message = `Plan has been renamed to ${newName}`;
            this.snackBar.open(message, undefined, {});
          }),
          catchError(() => {
            return of(new ChangePlanNameFailure({ roadmapId, oldName }));
          }),
        );
    }),
  );

  @Effect()
  createPlan$ = this.actions$.pipe(
    ofType<CreatePlan>(PlanActionTypes.CreatePlan),
    flatMap(action => {
      const { name, primary } = action.payload;
      return this.api.createDegreePlan(name, primary).pipe(
        flatMap(newPlan => {
          const newYears = loadPlanYears(
            this.api,
            newPlan.roadmapId,
            this.termCodeService,
          );

          return forkJoinWithKeys({
            newPlan: of(newPlan),
            newYears,
          });
        }),
        mergeMap(({ newPlan, newYears }) => [
          new CreatePlanSuccess({ newPlan, newYears }),
          new UpdateUserPreferences({
            degreePlannerSelectedPlan: newPlan.roadmapId,
          }),
        ]),
        tap(() => {
          const message = `New plan has been created`;
          this.snackBar.open(message, undefined, {});
        }),
        catchError(error => {
          return of(
            new PlanError({
              message: 'Unable to create new plan',
              duration: 2000,
              error,
            }),
          );
        }),
      );
    }),
  );

  @Effect()
  deletePlan$ = this.actions$.pipe(
    ofType<DeletePlan>(PlanActionTypes.DeletePlan),
    flatMap(action => {
      const { roadmapId } = action.payload;
      return this.api.deleteDegreePlan(roadmapId).pipe(
        map(() => new DeletePlanSuccess({ roadmapId })),
        tap(() => {
          const message = `Deleting selected plan`;
          this.snackBar.open(message, undefined, { duration: 10000 });
        }),
        catchError(error => {
          return of(
            new PlanError({
              message: 'Unable to delete plan',
              duration: 2000,
              error,
            }),
          );
        }),
      );
    }),
  );
}

type SimpleMap = { [name: string]: any };
type ObservableMap<T = SimpleMap> = { [K in keyof T]: Observable<T[K]> };

const forkJoinWithKeys = <T = SimpleMap>(pairs: ObservableMap<T>) => {
  const keys = Object.keys(pairs);
  const observables = keys.map(key => pairs[key]);
  return forkJoin(observables).pipe(
    map<any[], T>(values => {
      const valueMapping = {} as T;

      keys.forEach((key, index) => {
        valueMapping[key] = values[index];
      });

      return valueMapping;
    }),
  );
};

const unique = <T>(things: T[]): T[] => {
  return things.filter((thing, index, all) => all.indexOf(thing) === index);
};

const matchesTermCode = (termCode: TermCode) => (thing: {
  termCode: string;
}) => {
  return thing.termCode === termCode.toString();
};

const buildTerm = (
  roadmapId: number,
  termCode: TermCode,
  notes: ReadonlyArray<Note>,
  courses: ReadonlyArray<{
    termCode: string;
    courses: ReadonlyArray<CourseBase>;
  }>,
): PlannedTerm => {
  const baseNote = notes.find(matchesTermCode(termCode));
  const note: PlannedTermNote | undefined = baseNote
    ? { isLoaded: true, text: baseNote.note, id: baseNote.id }
    : undefined;
  const group = courses.find(matchesTermCode(termCode));
  const formattedCourses = (group ? group.courses : []).map(course => {
    return { ...course, termCode: termCode.toString() };
  });

  const plannedCourses: Course[] = [];
  const enrolledCourses: Course[] = [];

  formattedCourses.forEach(course => {
    if (course.studentEnrollmentStatus === 'Enrolled') {
      enrolledCourses.push(course);
      return;
    }
    plannedCourses.push(course);
  });

  return {
    roadmapId,
    termCode,
    note,
    plannedCourses,
    enrolledCourses,
  };
};

const loadPlanYears = (
  api: DegreePlannerApiService,
  roadmapId: number,
  termCodeService: TermCodeFactory,
): Observable<YearMapping> => {
  const notesAndCourses$ = forkJoinWithKeys({
    notes: api.getAllNotes(roadmapId),
    courses: api.getAllTermCourses(roadmapId),
  });

  const uniqueYearCodes$ = notesAndCourses$.pipe(
    map(({ notes, courses }) => {
      const noteTermCodes = notes.map(note => note.termCode);
      const courseTermCodes = courses.map(course => course.termCode);
      const allTermCodes = [
        ...noteTermCodes,
        ...courseTermCodes,
        ...termCodeService.active.map(t => t.toString()),
      ].map(t => termCodeService.fromString(t));
      const uniqueYearCodes = unique(
        allTermCodes.map(tc => tc.yearCode.toString()),
      ).map(yearCodeStr => termCodeService.fromRawYearCode(yearCodeStr));
      return {
        uniqueYearCodes,
        notes,
        courses,
      };
    }),
  );

  const visibleYears$ = uniqueYearCodes$.pipe(
    map(({ uniqueYearCodes, notes, courses }) => {
      const mapping: MutableYearMapping = {};
      uniqueYearCodes.forEach(yearCode => {
        const { fall, spring, summer } = termCodeService.fromYear(yearCode);
        mapping[yearCode.toString()] = {
          yearCode,
          isExpanded: !(fall.isPast() && spring.isPast() && summer.isPast()),
          fall: buildTerm(roadmapId, fall, notes, courses),
          spring: buildTerm(roadmapId, spring, notes, courses),
          summer: buildTerm(roadmapId, summer, notes, courses),
        };
      });

      return mapping as YearMapping;
    }),
  );

  return visibleYears$;
};

const pickPrimaryDegreePlan = (plans: DegreePlan[]): DegreePlan => {
  const primary = plans.find(plan => plan.primary);
  return primary ? primary : plans[0];
};

const pickDegreePlanById = (
  roadmapId: number,
  plans: DegreePlan[],
): DegreePlan => {
  const plan = plans.find(plan => plan.roadmapId === roadmapId);
  return plan ? plan : plans[0];
};
