import { createSelector } from '@ngrx/store';
import { GlobalState } from '@app/core/state';
import { DegreePlannerState } from './state';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';
import { YearCode } from '@app/degree-planner/shared/term-codes/yearcode';
import { YearMapping, Year } from '@app/core/models/year';

export const getDegreePlannerState = ({ degreePlanner }: GlobalState) => {
  return degreePlanner;
};

export const hasLoadedDegreePlan = createSelector(
  getDegreePlannerState,
  state => state.visibleDegreePlan !== undefined,
);

export const selectVisibleDegreePlan = createSelector(
  (state: GlobalState) => state.degreePlanner,
  state => state.visibleDegreePlan,
);

export const selectAllDegreePlans = createSelector(
  (state: GlobalState) => state.degreePlanner,
  state => state.allDegreePlans,
);

export const getSavedForLaterCourses = createSelector(
  (state: GlobalState) => state.degreePlanner,
  state => state.savedForLaterCourses,
);

export const selectAllVisibleYears = createSelector(
  (state: GlobalState) => state.degreePlanner,
  state => state.visibleYears,
);

export const selectYearExpandedState = createSelector(
  (state: GlobalState, params: { yearCode: YearCode }): Year | undefined => {
    return state.degreePlanner.visibleYears[params.yearCode.toString()];
  },
  year => {
    if (year) {
      return year.isExpanded;
    } else {
      return false;
    }
  },
);

export const selectVisibleTerm = createSelector(
  (state: GlobalState) => state.degreePlanner.visibleYears,
  (years: YearMapping, params: { termCode: TermCode }) => {
    const { yearCode, termName } = params.termCode;
    const year = years[yearCode.toString()] as Year | undefined;
    if (year) {
      return year[termName];
    } else {
      return undefined;
    }
  },
);

export const selectGradeVisibility = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => state.showGrades,
);

export const isCourseSearchOpen = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => {
    return state.search.visible;
  },
);

export const getSelectedSearchTerm = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => {
    return state.search.selectedTerm;
  },
);

export const getActiveSelectedSearchTerm = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => {
    if (state.search.selectedTerm && state.search.selectedTerm.isActive()) {
      return state.search.selectedTerm;
    } else {
      return undefined;
    }
  },
);

export const alerts = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => state.alerts,
);

export const isLoadingPlan = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => state.isLoadingPlan,
);

export const isSidenavOpen = createSelector(
  getDegreePlannerState,
  (state: DegreePlannerState) => state.isSidenavOpen,
);
