import { CourseDetails } from './../../core/models/course-details';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from './../../../environments/environment';
import { Note } from '@app/core/models/note';
import { Term } from '@app/core/models/term';
import { CourseBase, SubjectCodesTo } from '@app/core/models/course';
import { DegreePlan } from '@app/core/models/degree-plan';
import { Profile } from '@app/core/models/profile';
import { SavedForLaterCourseBase } from '@app/core/models/saved-for-later-course';
import { SearchResults } from '@app/core/models/search-results';
import { TermCode } from '../shared/term-codes/termcode';
import { StudentInfo } from '@app/core/models/student-info';
import { RawTermCode } from '../shared/term-codes/without-era';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

// const HTTP_OPTIONS = {
//   headers: new HttpHeaders({
//     'Content-Type': 'application/json',
//     'X-API-Key': '',
//     uid: '',
//     wiscEduISISEmplID: '',
//     wiscEduPVI: '',
//   }),
// };

@Injectable({ providedIn: 'root' })
export class DegreePlannerApiService {
  constructor(private http: HttpClient) {}

  public getSavedForLaterCourses(): Observable<SavedForLaterCourseBase[]> {
    return this.http.get<SavedForLaterCourseBase[]>(
      `${environment.apiPlannerUrl}/favorites`,
      HTTP_OPTIONS,
    );
  }

  public createDegreePlan(name: string, primary: boolean = false) {
    const url = `${environment.apiPlannerUrl}/degreePlan`;
    const payload = { name, primary };
    return this.http.post<DegreePlan>(url, payload);
  }

  public deleteDegreePlan(roadmapId: number) {
    const url = `${environment.apiPlannerUrl}/degreePlan/${roadmapId}`;
    return this.http.delete<void>(url);
  }

  public getUserProfile(): Observable<Profile> {
    // Prevents error locally
    if (environment.production) {
      return this.http.get<Profile>('/profile', HTTP_OPTIONS);
    } else {
      return of<Profile>();
    }
  }

  public getAllDegreePlans(): Observable<DegreePlan[]> {
    return this.http.get<DegreePlan[]>(this.degreePlanEndpoint(), HTTP_OPTIONS);
  }

  public getPlan(roadmapId: number): Observable<DegreePlan> {
    return this.http.get<DegreePlan>(
      this.degreePlanEndpoint(roadmapId),
      HTTP_OPTIONS,
    );
  }

  public getAllSubjects() {
    return this.http.get<SubjectCodesTo<string>>(
      this.searchEndpoint('subjectsMap/0000'),
      HTTP_OPTIONS,
    );
  }

  public getSubjectShortDescriptions() {
    const url = `/api/search/v1/subjectsMap/0000`;
    return this.http.get<SubjectCodesTo<string>>(url, HTTP_OPTIONS);
  }

  public getAllSubjectDescriptions(): Observable<{}> {
    return this.http.get(this.searchEndpoint('subjects'), HTTP_OPTIONS);
  }

  public getSubjectLongDescriptions() {
    interface Subjects {
      [key: string]: {
        termCode: string;
        subjectCode: string;
        formalDescription: string;
      }[];
    }

    const url = `/api/search/v1/subjects`;
    return this.http.get<Subjects>(url, HTTP_OPTIONS);
  }

  public getActiveTerms(): Observable<Term[]> {
    return this.http.get<Term[]>(this.searchEndpoint('terms'));
  }

  public getActiveTermCodes() {
    const url = `/api/search/v1/terms`;
    return this.http
      .get<Term[]>(url, HTTP_OPTIONS)
      .pipe(map(terms => terms.map(term => new RawTermCode(term.termCode))));
  }

  public getAllNotes(roadmapId: number): Observable<Note[]> {
    return this.http.get<Note[]>(
      this.degreePlanEndpoint(roadmapId, 'notes'),
      HTTP_OPTIONS,
    );
  }

  getCourseDetails(
    subjectCode: string,
    courseId: string,
  ): Observable<CourseDetails> {
    return this.http.get<CourseDetails>(
      environment.apiSearchUrl + '/course/0000/' + subjectCode + '/' + courseId,
      HTTP_OPTIONS,
    );
  }

  public getAllTermCourses(
    roadmapId: number,
  ): Observable<{ termCode: string; courses: CourseBase[] }[]> {
    return this.http.get<{ termCode: string; courses: CourseBase[] }[]>(
      this.degreePlanEndpoint(roadmapId, 'termcourses'),
      HTTP_OPTIONS,
    );
  }

  public updateCourseTerm(
    roadmapId: number,
    recordId: number,
    termCode: TermCode,
  ): Observable<any> {
    return this.http.put<CourseBase>(
      environment.apiPlannerUrl +
        '/degreePlan/' +
        roadmapId +
        '/courses/' +
        recordId +
        '?termCode=' +
        termCode.toString(),
      HTTP_OPTIONS,
    );
  }

  public autocomplete(search: string) {
    const data = {
      subjectCode: '104',
      termCode: '0000',
      matchText: search,
    };

    return this.http.post('/api/search/v1/autocomplete', data, HTTP_OPTIONS);
  }

  public searchCourses(config: {
    subjectCode: string;
    searchText?: string;
    termCode?: string;
  }) {
    const { subjectCode, termCode, searchText } = config;

    const payload: any = {
      selectedTerm: termCode,
      queryString: searchText === '' ? '*' : searchText,

      // Filters are use to build the elastic search query
      filters: [],

      // These options control how much data we get back
      page: 1,
      pageSize: 100,
      sortOrder: 'SCORE',
    };

    // If we have a specific subject code, add a fitler for it
    if (subjectCode !== '-1') {
      payload.filters.push({ term: { 'subject.subjectCode': subjectCode } });
    }

    // 0000 is used to search all courses
    // Any other term code we can assuem is an active term
    if (termCode !== '0000') {
      // Used to search a specific term
      payload.filters.push({
        has_child: {
          type: 'enrollmentPackage',
          query: {
            // We want to make sure we search for ALL classes regardless of status
            match: {
              'packageEnrollmentStatus.status': 'OPEN WAITLISTED CLOSED',
            },
          },
        },
      });
    }

    return this.http.post<SearchResults>(
      '/api/search/v1',
      payload,
      HTTP_OPTIONS,
    );
  }

  public addCourse(
    planId: number,
    subjectCode: string,
    courseId: string,
    termCode: TermCode,
  ): Observable<CourseBase> {
    return this.http.post<CourseBase>(
      environment.apiPlannerUrl + '/degreePlan/' + planId + '/courses',
      { subjectCode, courseId, termCode: termCode.toString() },
      HTTP_OPTIONS,
    );
  }

  public addCourseToCart(
    subjectCode: string,
    courseId: string,
    termCode: TermCode,
  ): Observable<CourseBase> {
    return this.http.post<CourseBase>(
      environment.apiPlannerUrl +
        '/roadmap/' +
        termCode +
        '/' +
        subjectCode +
        '/' +
        courseId,
      {},
      HTTP_OPTIONS,
    );
  }

  public removeCourse(planId: number, recordId: number): Observable<void> {
    return this.http.delete<void>(
      environment.apiPlannerUrl +
        '/degreePlan/' +
        planId +
        '/courses/' +
        recordId,
      HTTP_OPTIONS,
    );
  }

  public updateCourse(
    planId: number,
    recordId: number,
    termCode: string,
    options: { credits: number },
  ): Observable<void> {
    // prettier-ignore
    const url = `${environment.apiPlannerUrl}/degreePlan/${planId}/courses/${recordId}?termCode=${termCode}`;
    console.log(options);
    return this.http.put<void>(
      url,
      {
        credits: options.credits,
      },
      HTTP_OPTIONS,
    );
  }

  public saveForLater(
    subjectCode: string,
    courseId: string,
  ): Observable<SavedForLaterCourseBase> {
    return this.http.post<SavedForLaterCourseBase>(
      environment.apiPlannerUrl + '/favorites/' + subjectCode + '/' + courseId,
      HTTP_OPTIONS,
    );
  }

  public removeSavedForLater(
    subjectCode: string,
    courseId: string,
  ): Observable<SavedForLaterCourseBase> {
    return this.http.delete<SavedForLaterCourseBase>(
      environment.apiPlannerUrl + '/favorites/' + subjectCode + '/' + courseId,
      HTTP_OPTIONS,
    );
  }

  public createNote(
    planId: number,
    termCode: TermCode,
    noteText: string,
  ): Observable<Note> {
    const payload = {
      termCode: termCode.toString(),
      note: noteText,
    };

    return this.http.post<Note>(
      this.degreePlanEndpoint(planId, 'notes'),
      payload,
      HTTP_OPTIONS,
    );
  }

  public updateNote(
    planId: number,
    termCode: TermCode,
    noteText: string,
    noteId: number,
  ): Observable<Note> {
    const payload = {
      termCode: termCode.toString(),
      note: noteText,
      id: noteId,
    };

    return this.http
      .put<null>(
        this.degreePlanEndpoint(planId, 'notes', noteId),
        payload,
        HTTP_OPTIONS,
      )
      .pipe(map(() => payload));
  }

  public deleteNote(planId: number, noteId: number): Observable<null> {
    return this.http.delete<null>(
      this.degreePlanEndpoint(planId, 'notes', noteId),
      HTTP_OPTIONS,
    );
  }

  public updatePlan(
    planId: number,
    name: string,
    primary: boolean,
  ): Observable<1> {
    return this.http.put<1>(
      this.degreePlanEndpoint(planId),
      { name, primary },
      HTTP_OPTIONS,
    );
  }

  public getUserPreferences(): Observable<{ [key: string]: any }> {
    return this.http.get(
      `${environment.apiPlannerUrl}/preferences`,
      HTTP_OPTIONS,
    );
  }

  public updateUserPreferences(payload: {
    [key: string]: any;
  }): Observable<{ [key: string]: any }> {
    return this.http.post(
      `${environment.apiPlannerUrl}/preferences`,
      payload,
      HTTP_OPTIONS,
    );
  }

  public getStudentInfo() {
    const url = `${environment.apiEnrollUrl}/studentInfo`;
    // return this.http.get<StudentInfo>(url, HTTP_OPTIONS);
    return of({});
  }

  /**
   * Helper function for building API endpoint URLs
   */
  private degreePlanEndpoint(...parts: any[]): string {
    return ['degreePlan']
      .concat(parts.map(part => part.toString()))
      .reduce((soFar, next) => {
        return Location.joinWithSlash(soFar, next);
      }, environment.apiPlannerUrl);
  }

  private searchEndpoint(...parts: any[]): string {
    return parts
      .map(part => part.toString())
      .reduce((soFar, next) => {
        return Location.joinWithSlash(soFar, next);
      }, environment.apiSearchUrl);
  }
}
