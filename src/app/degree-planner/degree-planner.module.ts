import { NgModule } from '@angular/core';
import { DegreePlannerViewComponent } from './degree-planner-view/degree-planner-view.component';
import { SharedModule } from '@app/shared/shared.module';
import { TermContainerComponent } from './term-container/term-container.component';
import { SidenavMenuItemComponent } from './sidenav-menu-item/sidenav-menu-item.component';
import { SavedForLaterContainerComponent } from './saved-for-later-container/saved-for-later-container.component';
import { CourseItemComponent } from './shared/course-item/course-item.component';
import { NotesDialogComponent } from './dialogs/notes-dialog/notes-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { RemoveCourseConfirmDialogComponent } from './dialogs/remove-course-confirm-dialog/remove-course-confirm-dialog.component';
import { YearContainerComponent } from '@app/degree-planner/year-container/year-container.component';
import { CourseSearchComponent } from '@app/degree-planner/course-search/course-search.component';
import { EffectsModule } from '@ngrx/effects';
import { DegreePlanEffects } from './store/effects/plan.effects';
import { NoteEffects } from './store/effects/note.effects';
import { CourseEffects } from './store/effects/course.effects';
import { ErrorEffects } from './store/effects/error.effects';
import { StoreModule } from '@ngrx/store';
import { degreePlannerReducer } from './store/reducer';

@NgModule({
  imports: [
    SharedModule,
    DragDropModule,
    StoreModule.forFeature('degreePlanner', degreePlannerReducer),
    EffectsModule.forRoot([
      DegreePlanEffects,
      NoteEffects,
      CourseEffects,
      ErrorEffects,
    ]),
  ],
  exports: [DragDropModule],
  declarations: [
    DegreePlannerViewComponent,
    TermContainerComponent,
    CourseItemComponent,
    SidenavMenuItemComponent,
    SavedForLaterContainerComponent,
    NotesDialogComponent,
    RemoveCourseConfirmDialogComponent,
    YearContainerComponent,
    CourseSearchComponent,
  ],
  entryComponents: [NotesDialogComponent, RemoveCourseConfirmDialogComponent],
})
export class DegreePlannerModule {}
