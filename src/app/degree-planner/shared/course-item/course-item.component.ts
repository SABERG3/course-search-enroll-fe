import { MediaMatcher } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as utils from '@app/degree-planner/shared/utils';
import {
  AddCourse,
  AddSaveForLater,
  MoveCourseBetweenTerms,
  RemoveSaveForLater,
  RemoveCourse,
} from '@app/degree-planner/store/actions/course.actions';
import { CloseCourseSearch } from '@app/degree-planner/store/actions/ui.actions';
import { GlobalState } from '@app/core/state';
import { Course } from '@app/core/models/course';
import * as selectors from '@app/degree-planner/store/selectors';
import { DegreePlannerApiService } from '@app/degree-planner/services/api.service';
import { ConfirmDialogComponent } from '@app/shared/dialogs/confirm-dialog/confirm-dialog.component';
import { CourseDetailsDialogComponent } from '@app/degree-planner/dialogs/course-details-dialog/course-details-dialog.component';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import { TermCode, Era } from '@app/degree-planner/shared/term-codes/termcode';
import { PlannedTerm } from '@app/core/models/planned-term';
import { ConstantsService } from '@app/degree-planner/services/constants.service';
import { TermCodeFactory } from '@app/degree-planner/services/termcode.factory';

const isntUndefined = <T>(thing: T | undefined): thing is T => {
  return thing !== undefined;
};

@Component({
  selector: 'cse-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.scss'],
})
export class CourseItemComponent implements OnInit {
  @Input() course: Course;
  @Input() isCurrentTerm: boolean;
  @Input() isPastTerm: boolean;
  @Input() disabled: boolean;
  @Input() type: 'saved' | 'course' | 'search';
  @Input() era?: Era;
  visibleTerms: any;
  activeTerm: any;
  public status:
    | 'InProgress'
    | 'Waitlisted'
    | 'NotOfferedInTerm'
    | 'DoesNotExist'
    | 'Normal';
  public droppableTermCodes$: Observable<TermCode[]>;
  public term$: Observable<PlannedTerm>;
  public plannedCourses: ReadonlyArray<Course>;
  public toActiveTerm: boolean;
  public mobileView: MediaQueryList;
  public showGrades$: Observable<boolean>;

  constructor(
    private api: DegreePlannerApiService,
    public dialog: MatDialog,
    private store: Store<GlobalState>,
    private constants: ConstantsService,
    private snackBar: MatSnackBar,
    public mediaMatcher: MediaMatcher,
    private termCodeFactory: TermCodeFactory,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 959px)');
  }

  ngOnInit() {
    const isActive = this.era === Era.Active;
    const isNotOffered = this.course.studentEnrollmentStatus === 'NOTOFFERED';
    const doesNotExist = this.course.studentEnrollmentStatus === 'DOESNOTEXIST';

    const isWaitlisted = this.course.studentEnrollmentStatus === 'Waitlisted';
    const isInProgress =
      isActive && this.course.studentEnrollmentStatus === 'Enrolled';

    if (isWaitlisted) {
      this.status = 'Waitlisted';
    } else if (isInProgress) {
      this.status = 'InProgress';
    } else if (isNotOffered) {
      this.status = 'NotOfferedInTerm';
    } else if (doesNotExist) {
      this.status = 'DoesNotExist';
    } else {
      this.status = 'Normal';
    }

    this.showGrades$ = this.store.pipe(select(selectors.selectGradeVisibility));
  }

  onMenuOpen() {
    this.droppableTermCodes$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      utils.yearsToDroppableTermCodes(),
      distinctUntilChanged(utils.compareArrays((a, b) => a.equals(b))),
    );
  }

  moveToSavedForLater(course) {
    this.store.dispatch(
      new AddSaveForLater({
        courseId: course.courseId,
        subjectCode: course.subjectCode,
        title: course.title,
        catalogNumber: course.catalogNumber,
        newIndex: 0,
      }),
    );
  }

  /**
   *
   *  Handle moving a course to different terms based on course type
   *
   */
  onMove(toTermCode: TermCode) {
    this.term$ = this.store.pipe(
      select(selectors.selectVisibleTerm, { termCode: toTermCode }),
      filter(isntUndefined),
      distinctUntilChanged(),
    );

    this.term$.subscribe(term => {
      this.plannedCourses = term.plannedCourses;
      this.toActiveTerm = term.termCode.era === Era.Active;
    });

    const isCourseInPlannedCourses = this.plannedCourses.some(
      course => course.courseId === this.course.courseId,
    );

    if (isCourseInPlannedCourses) {
      this.dialog
        .open(ConfirmDialogComponent, {
          data: {
            title: `Can't add course`,
            confirmText: 'OK',
            dialogClass: 'alertDialog',
            text: 'This course already exists in selected term',
          },
        })
        .afterClosed();
      return;
    }

    switch (this.type) {
      case 'course': {
        const id = this.course.id as number;
        const { subjectCode, courseId } = this.course;
        const from = this.termCodeFactory.fromString(this.course.termCode);
        this.store.dispatch(
          new MoveCourseBetweenTerms({
            to: toTermCode,
            from,
            id,
            newIndex: 0,
            courseId,
            subjectCode,
          }),
        );
        break;
      }

      case 'saved': {
        const { subjectCode, courseId } = this.course;
        this.addToTerm(toTermCode);
        this.store.dispatch(new RemoveSaveForLater({ subjectCode, courseId }));
        break;
      }

      case 'search': {
        this.addToTerm(toTermCode);
        if (this.mobileView.matches) {
          this.store.dispatch(new CloseCourseSearch());
        }
        break;
      }
    }
  }

  /**
   *
   *  Handle saving a course for later (This is not possible if a course is already saved)
   *
   */
  onSaveForLater() {
    const {
      courseId,
      subjectCode,
      title,
      catalogNumber,
      termCode,
    } = this.course;

    // Dispatch a save for later event
    this.store.dispatch(
      new AddSaveForLater({
        courseId: courseId,
        subjectCode: subjectCode,
        title: title,
        catalogNumber: catalogNumber,
        newIndex: 0,
      }),
    );

    // If course is in a term, we need to remove it
    // If this course came from search and is mobile, close the search
    switch (this.type) {
      case 'course':
        this.store.dispatch(
          new RemoveCourse({
            fromTermCode: this.termCodeFactory.fromString(termCode),
            recordId: this.course.id as number,
          }),
        );
        break;

      case 'search':
        if (this.mobileView.matches) {
          this.store.dispatch(new CloseCourseSearch());
        }
        break;
    }
  }

  /**
   *
   *  Handle removing a course (This is not possible for type 'search')
   *
   */
  onRemove() {
    const dialogOptions = {
      title: 'Remove Course?',
      text: '',
      confirmText: 'Remove Course',
      confirmColor: 'accent',
    };

    switch (this.type) {
      case 'saved':
        dialogOptions.text = `This will remove "${this.course.title}" from your saved courses.`;
        break;

      default:
        if (this.era === Era.Future) {
          dialogOptions.text = `This will remove "${this.course.title}" from your degree plan.`;
        } else {
          dialogOptions.text = `This will remove "${this.course.title}" from your degree plan and your cart.`;
        }
    }

    this.dialog
      .open(ConfirmDialogComponent, { data: dialogOptions })
      .afterClosed()
      .subscribe((result: { confirmed: boolean }) => {
        // If the user confirmed the removal, remove course
        if (result && result.confirmed) {
          switch (this.type) {
            case 'course':
              this.store.dispatch(
                new RemoveCourse({
                  fromTermCode: this.termCodeFactory.fromString(
                    this.course.termCode,
                  ),
                  recordId: this.course.id as number,
                }),
              );
              break;

            case 'saved':
              const { subjectCode, courseId } = this.course;
              this.store.dispatch(
                new RemoveSaveForLater({ subjectCode, courseId }),
              );
              break;
          }
        }
      });
  }

  addToTerm(toTermCode: TermCode) {
    this.store.dispatch(
      new AddCourse({
        courseId: this.course.courseId,
        termCode: toTermCode,
        subjectCode: this.course.subjectCode,
        title: this.course.title,
        catalogNumber: this.course.catalogNumber,
      }),
    );
  }

  openCourseDetailsDialog() {
    const { subjectCode, courseId, catalogNumber } = this.course;
    if (this.course.studentEnrollmentStatus === 'DOESNOTEXIST') {
      const { short } = this.constants.subjectDescription(subjectCode);
      this.snackBar.open(`'${short} ${catalogNumber}' no longer offered`);
      return;
    }

    this.dialog.open(CourseDetailsDialogComponent, {
      maxWidth: '800px',
      width: '80%',
      panelClass: this.mobileView.matches ? 'dialog-fullscreen' : '',
      data: { subjectCode, courseId, type: this.type },
      closeOnNavigation: true,
    });
  }

  // Check for enter key presses
  detectEnter($event) {
    if ($event.keyCode === 13) {
      this.openCourseDetailsDialog();
    }
  }
}
