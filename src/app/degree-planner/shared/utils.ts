import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { YearMapping } from '@app/core/models/year';
import { TermCode, Era } from './term-codes/termcode';

export const yearsToDropZoneIds = () => (years$: Observable<YearMapping>) => {
  return years$.pipe(
    map(years => {
      const yearCodes = Object.keys(years);
      const termCodes = yearCodes.reduce<string[]>((acc, yearCode) => {
        if (years[yearCode].fall.termCode.era !== Era.Past) {
          acc = acc.concat(years[yearCode].fall.termCode.toString());
        }

        if (years[yearCode].spring.termCode.era !== Era.Past) {
          acc = acc.concat(years[yearCode].spring.termCode.toString());
        }

        if (years[yearCode].summer.termCode.era !== Era.Past) {
          acc = acc.concat(years[yearCode].summer.termCode.toString());
        }

        return acc;
      }, []);

      const termIds = [
        'saved-courses',
        ...termCodes.map(termCode => `term-${termCode}`),
      ];
      return termIds;
    }),
  );
};

export const compareArrays = <T>(cmp: (a: T, b: T) => boolean) => {
  return (a: T[], b: T[]): boolean => {
    if (a.length === b.length) {
      return a.every((elem, i) => cmp(elem, b[i]));
    } else {
      return false;
    }
  };
};

export const compareStringArrays = compareArrays<string>((a, b) => a === b);

export const yearsToDroppableTermCodes = () => (
  years$: Observable<YearMapping>,
) => {
  return years$.pipe(
    map(years => {
      const yearCodes = Object.keys(years);
      return yearCodes.reduce<TermCode[]>((acc, yearCode) => {
        if (years[yearCode].fall.termCode.era !== Era.Past) {
          acc = acc.concat(years[yearCode].fall.termCode);
        }

        if (years[yearCode].spring.termCode.era !== Era.Past) {
          acc = acc.concat(years[yearCode].spring.termCode);
        }

        if (years[yearCode].summer.termCode.era !== Era.Past) {
          acc = acc.concat(years[yearCode].summer.termCode);
        }

        return acc;
      }, []);
    }),
  );
};

interface SimpleMap {
  [name: string]: any;
}

type ObservableMap<T = SimpleMap> = { [K in keyof T]: Observable<T[K]> };

export const forkJoinWithKeys = <T = SimpleMap>(pairs: ObservableMap<T>) => {
  const keys = Object.keys(pairs);
  const observables = keys.map(key => pairs[key]);
  return forkJoin(observables).pipe(
    map<any[], T>(values => {
      const valueMapping = {} as T;

      keys.forEach((key, index) => {
        valueMapping[key] = values[index];
      });

      return valueMapping;
    }),
  );
};
