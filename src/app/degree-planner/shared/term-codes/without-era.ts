const YEARCODE_PATTERN = /^[01]\d{2}$/i;
const TERMCODE_PATTERN = /^[01]\d{2}[2346]$/i;

export class RawYearCode {
  public readonly centuryOffset: '0' | '1';
  public readonly yearOffset: string;

  public static isValid(str: string): boolean {
    return YEARCODE_PATTERN.test(str);
  }

  public static sort(a: RawYearCode, b: RawYearCode): -1 | 0 | 1 {
    if (a.comesBefore(b)) {
      return -1;
    }

    if (a.comesAfter(b)) {
      return 1;
    }

    return 0;
  }

  public static fromString(str: string): RawYearCode {
    return new RawYearCode(str);
  }

  public get fromYear(): number {
    const century = 1900 + parseInt(this.centuryOffset, 10) * 100;
    const year = century + parseInt(this.yearOffset, 10) - 1;
    return year;
  }

  public get toYear(): number {
    return this.fromYear + 1;
  }

  public fall() {
    return new RawTermCode(`${this.toString()}2`);
  }

  public spring() {
    return new RawTermCode(`${this.toString()}4`);
  }

  public summer() {
    return new RawTermCode(`${this.toString()}6`);
  }

  constructor(str: string) {
    if (typeof str !== 'string') {
      throw new Error(`constructor expected string, got ${typeof str}`);
    }

    if (RawYearCode.isValid(str) === false) {
      throw new Error(`'${str}' is not a valid year code`);
    }

    this.centuryOffset = str.substr(0, 1) as RawYearCode['centuryOffset'];
    this.yearOffset = str.substr(1, 2) as RawYearCode['yearOffset'];
  }

  public equals(other: RawYearCode): boolean {
    return this.toString() === other.toString();
  }

  public comesBefore(other: RawYearCode): boolean {
    return this.toString() < other.toString();
  }

  public comesAfter(other: RawYearCode): boolean {
    return this.toString() > other.toString();
  }

  public toJSON() {
    return {
      centuryOffset: this.centuryOffset,
      yearOffset: this.yearOffset,
    };
  }

  public toString() {
    return `${this.centuryOffset}${this.yearOffset}`;
  }
}

export class RawTermCode {
  public readonly yearCode: RawYearCode;
  public readonly termId: '2' | '3' | '4' | '6';

  public static describe(str: string): string {
    return new RawTermCode(str).description;
  }

  public static isValid(str: string): boolean {
    return TERMCODE_PATTERN.test(str);
  }

  public static sort(a: RawTermCode, b: RawTermCode): -1 | 0 | 1 {
    if (a.comesBefore(b)) {
      return -1;
    }

    if (a.comesAfter(b)) {
      return 1;
    }

    return 0;
  }

  public get termName(): 'fall' | 'spring' | 'summer' {
    switch (this.termId) {
      case '2':
      case '3':
        return 'fall';
      case '4':
        return 'spring';
      case '6':
        return 'summer';
      default:
        throw new Error(`'${this.termId}' is not a valid term id`);
    }
  }

  public get description(): string {
    const capital = this.termName[0].toUpperCase();
    const rest = this.termName.slice(1);
    if (this.termName === 'fall') {
      return `${capital}${rest} ${this.yearCode.fromYear}`;
    } else {
      return `${capital}${rest} ${this.yearCode.toYear}`;
    }
  }

  constructor(str: string) {
    if (typeof str !== 'string') {
      throw new Error(`constructor expected string, got ${typeof str}`);
    }

    if (RawTermCode.isValid(str) === false) {
      throw new Error(`'${str}' is not a valid term code`);
    }

    this.yearCode = new RawYearCode(str.substr(0, 3));
    this.termId = str.substr(3, 1) as RawTermCode['termId'];
  }

  public equals(other: RawTermCode): boolean {
    return this.toString() === other.toString();
  }

  public comesBefore(other: RawTermCode): boolean {
    return this.toString() < other.toString();
  }

  public comesAfter(other: RawTermCode): boolean {
    return this.toString() > other.toString();
  }

  public toJSON() {
    return {
      yearCode: this.yearCode,
      termId: this.termId,
    };
  }

  public toString(): string {
    return `${this.yearCode}${this.termId}`;
  }
}
