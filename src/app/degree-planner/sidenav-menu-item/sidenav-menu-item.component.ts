import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { GlobalState } from '@app/core/state';
import { Store, select } from '@ngrx/store';
import { PromptDialogComponent } from '@app/shared/dialogs/prompt-dialog/prompt-dialog.component';
import { CreatePlan } from '@app/degree-planner/store/actions/plan.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { AddAcademicYear } from '../store/actions/ui.actions';

@Component({
  selector: 'cse-sidenav-menu-item',
  templateUrl: './sidenav-menu-item.component.html',
  styleUrls: ['./sidenav-menu-item.component.scss'],
})
export class SidenavMenuItemComponent implements OnDestroy {
  public inputForm: FormGroup;
  public yearCodes$: Observable<string[]>;
  public activeRoadmapId: Subscription;
  public planId: number;
  public showGrades$: Observable<boolean>;
  constructor(
    private store: Store<GlobalState>,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {
    this.activeRoadmapId = this.store
      .pipe(
        select(selectors.selectVisibleDegreePlan),
        filter(isntUndefined),
        map(plan => plan.roadmapId),
      )
      .subscribe(planId => {
        this.planId = planId;
      });
    this.showGrades$ = this.store.pipe(select(selectors.selectGradeVisibility));
  }
  // Unsubscribe from subs to prevent memeory leaks
  public ngOnDestroy() {
    this.activeRoadmapId.unsubscribe();
  }

  public onAddAcademicYear() {
    this.store.dispatch(new AddAcademicYear());
    this.snackBar.open('New academic year has been created');
  }

  public onCreatePlanClick() {
    this.dialog
      .open(PromptDialogComponent, {
        data: {
          title: 'Add degree plan',
          confirmText: 'Create plan',
          inputName: 'Plan name',
          maxLength: 100,
        },
      })
      .afterClosed()
      .subscribe((result: { confirmed: boolean; value: string }) => {
        const { confirmed, value } = result;
        if (confirmed) {
          const action = new CreatePlan({ name: value, primary: false });
          this.store.dispatch(action);
        }
      });
  }
}

const isntUndefined = <T>(anything: T | undefined): anything is T => {
  return anything !== undefined;
};
