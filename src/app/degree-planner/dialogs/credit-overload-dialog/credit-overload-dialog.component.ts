import { Component, Inject } from '@angular/core';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'cse-credit-overload-dialog',
  templateUrl: './credit-overload-dialog.component.html',
})
export class CreditOverloadDialogComponent {
  public termName: TermCode['termName'];
  public maxCredits: number;

  constructor(@Inject(MAT_DIALOG_DATA) data: any) {
    this.termName = data.termName;
    this.maxCredits = data.maxCredits;
  }
}
