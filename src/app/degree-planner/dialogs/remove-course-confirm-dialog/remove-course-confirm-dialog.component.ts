import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Course } from '@app/core/models/course';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import { Store } from '@ngrx/store';
import { RemoveCourse } from '@app/degree-planner/store/actions/course.actions';
import { TermCodeFactory } from '@app/degree-planner/services/termcode.factory';

@Component({
  selector: 'cse-remove-course-confirm-dialog',
  templateUrl: './remove-course-confirm-dialog.component.html',
  styleUrls: ['./remove-course-confirm-dialog.component.scss'],
})
export class RemoveCourseConfirmDialogComponent implements OnInit {
  course: Course;
  savedForLater: Boolean;
  courses: Course[];
  type: 'saved' | 'course' | 'search';

  constructor(
    private termCodeService: TermCodeFactory,
    private store: Store<{ degreePlanner: DegreePlannerState }>,
    @Inject(MAT_DIALOG_DATA) data: any,
  ) {
    this.course = data.course;
    this.type = data.type;
  }

  ngOnInit() {}

  removeCourse() {
    switch (this.type) {
      case 'saved':
        break;

      case 'course':
        const id = this.course.id;
        if (typeof id === 'number') {
          const fromTermCode = this.termCodeService.fromString(
            this.course.termCode,
          );
          this.store.dispatch(new RemoveCourse({ fromTermCode, recordId: id }));
        } else {
          throw new Error('cannot remove a course that does not have an ID');
        }
        break;
    }
  }
}
