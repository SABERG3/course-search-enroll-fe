import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/core/state';
import {
  WriteNote,
  DeleteNote,
} from '@app/degree-planner/store/actions/note.actions';
import { TermCode } from '@app/degree-planner/shared/term-codes/termcode';

export type NotesDialogData =
  | {
      termCode: TermCode;
      hasExistingNote: true;
      initialText: string;
      noteId: number;
    }
  | {
      termCode: TermCode;
      hasExistingNote: false;
      initialText?: never;
      noteId?: never;
    };

@Component({
  selector: 'cse-notes-dialog',
  templateUrl: './notes-dialog.component.html',
  styleUrls: ['./notes-dialog.component.scss'],
})
export class NotesDialogComponent implements OnInit {
  public form: FormGroup;
  public MAX_LENGTH = 512;

  constructor(
    private store: Store<GlobalState>,
    private dialogRef: MatDialogRef<NotesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NotesDialogData,
  ) {}

  public ngOnInit() {
    this.form = new FormGroup({
      textarea: new FormControl(this.data.initialText || '', [
        Validators.maxLength(this.MAX_LENGTH),
        Validators.required,
      ]),
    });
  }

  public writeNote() {
    const termCode = this.data.termCode;
    const noteText =
      typeof this.form.value.textarea === 'string'
        ? this.form.value.textarea
        : '';
    this.store.dispatch(new WriteNote({ termCode, noteText }));
    this.dialogRef.close({ event: 'save' });
  }

  public deleteNote() {
    const termCode = this.data.termCode;
    if (this.data.noteId !== undefined) {
      const noteId = this.data.noteId;
      this.store.dispatch(new DeleteNote({ termCode, noteId }));
      this.dialogRef.close({ event: 'remove' });
    }
  }
}
