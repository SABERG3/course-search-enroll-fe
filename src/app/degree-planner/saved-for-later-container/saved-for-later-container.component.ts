import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as utils from '@app/degree-planner/shared/utils';
import { SavedForLaterCourse } from '@app/core/models/saved-for-later-course';
import { Course } from '@app/core/models/course';
import { DegreePlannerState } from '@app/degree-planner/store/state';
import {
  AddSaveForLater,
  RemoveCourse,
  MoveCourseInsideSFL,
} from '@app/degree-planner/store/actions/course.actions';
import * as selectors from '@app/degree-planner/store/selectors';
import { distinctUntilChanged } from 'rxjs/operators';
import { MediaMatcher } from '@angular/cdk/layout';
import { TermCodeFactory } from '../services/termcode.factory';

@Component({
  selector: 'cse-saved-for-later-container',
  templateUrl: './saved-for-later-container.component.html',
  styleUrls: ['./saved-for-later-container.component.scss'],
})
export class SavedForLaterContainerComponent implements OnInit {
  public courses$: Observable<ReadonlyArray<SavedForLaterCourse>>;
  public dropZoneIds$: Observable<string[]>;
  public mobileView: MediaQueryList;
  public hasItemDraggedOver: boolean;

  constructor(
    private store: Store<{ degreePlanner: DegreePlannerState }>,
    private termCodeService: TermCodeFactory,
    mediaMatcher: MediaMatcher,
  ) {
    this.mobileView = mediaMatcher.matchMedia('(max-width: 900px)');
  }

  public ngOnInit() {
    this.hasItemDraggedOver = false;
    this.dropZoneIds$ = this.store.pipe(
      select(selectors.selectAllVisibleYears),
      utils.yearsToDropZoneIds(),
      distinctUntilChanged(utils.compareStringArrays),
    );

    this.courses$ = this.store.pipe(select(selectors.getSavedForLaterCourses));
  }

  drop(event: any) {
    const newContainerId = event.container.id;
    const previousContainerId = event.previousContainer.id;
    const fromTerm = previousContainerId.indexOf('term-') === 0;
    const fromDifferentContainer = newContainerId !== previousContainerId;

    this.hasItemDraggedOver = false;

    if (fromDifferentContainer && fromTerm) {
      const course = event.item.data as Course;

      this.store.dispatch(
        new AddSaveForLater({
          courseId: course.courseId,
          subjectCode: course.subjectCode,
          title: course.title,
          catalogNumber: course.catalogNumber,
          newIndex: event.currentIndex,
        }),
      );

      this.store.dispatch(
        new RemoveCourse({
          fromTermCode: this.termCodeService.fromString(course.termCode),
          recordId: course.id as number,
        }),
      );
    } else if (newContainerId === previousContainerId) {
      const course = event.item.data as SavedForLaterCourse;
      this.store.dispatch(
        new MoveCourseInsideSFL({
          courseId: course.courseId,
          newIndex: event.currentIndex,
        }),
      );
    }
  }

  dragEnter() {
    this.hasItemDraggedOver = true;
  }

  dragExit() {
    this.hasItemDraggedOver = false;
  }
}
