import { Observable, of } from 'rxjs';
import { Note } from './../../core/models/note';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core/core.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { TermContainerComponent } from './term-container.component';
import { PlannedTerm } from '@app/core/models/planned-term';

describe('TermContainerComponent', () => {
  let termComponent: TermContainerComponent;
  let fixture: ComponentFixture<TermContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        CoreModule,
        SharedModule,
      ],
      declarations: [TermContainerComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermContainerComponent);
    termComponent = fixture.componentInstance;
    const term: PlannedTerm = {
      termCode: '1194',
      era: 'active',
      note: undefined,
      courses: [],
    };
    termComponent.term$ = of(term);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(termComponent).toBeTruthy();
  });
});
