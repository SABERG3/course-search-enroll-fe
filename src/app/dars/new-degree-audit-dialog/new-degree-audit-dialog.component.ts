import { Component, OnInit, OnDestroy } from '@angular/core';
import { DarsApiService } from '../services/api.service';
import { DegreePrograms } from '../models/degree-program';
import { StudentDegreeProgram } from '../models/student-degree-program';
import { Observable, combineLatest, Subscription } from 'rxjs';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray,
  AbstractControl,
} from '@angular/forms';
import { HonorsOption } from '../models/honors-option';
import {
  map,
  filter,
  flatMap,
  shareReplay,
  distinctUntilChanged,
  tap,
} from 'rxjs/operators';
import { CourseBase } from '@app/core/models/course';
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/core/state';
import { degreePlans } from '../store/selectors';
import { DegreePlan } from '@app/core/models/degree-plan';
import { MatDialogRef } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { TermCodeFactory } from '@app/degree-planner/services/termcode.factory';

const inclusiveRange = (from: number, to: number) => {
  const range: number[] = [];
  for (let i = from; i <= to; i++) {
    range.push(i);
  }
  return range;
};

export interface NewDegreeAuditFields {
  roadmapId: number;
  darsInstitutionCode: string;
  darsDegreeProgramCode: string;
  degreePlannerPlanName: string;
  darsHonorsOptionCode: string;
  whichEnrolledCoursesIncluded: string;
  fixedCredits: {
    termCode: string;
    subjectCode: string;
    recordId: number;
    credits: number;
  }[];
}

type CourseWithCreditRange = CourseBase & { range: number[] };

@Component({
  selector: 'cse-new-degree-audit-dialog',
  templateUrl: './new-degree-audit-dialog.component.html',
  styleUrls: ['./new-degree-audit-dialog.component.scss'],
})
export class NewDegreeAuditDialogComponent implements OnInit, OnDestroy {
  // Form-builder objects
  public chosenProgram: FormControl;
  public chosenAuditSettings: FormGroup;
  public chosenCreditSettings: FormArray;

  // API observables
  public degreePrograms$: Observable<StudentDegreeProgram[]>;
  public degreePlans$: Observable<DegreePlan[]>;
  public honorsOptions$: Observable<HonorsOption[]>;
  public variableCreditCourses$: Observable<CourseWithCreditRange[]>;
  public variableCreditCoursesSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private api: DarsApiService,
    private termCodeService: TermCodeFactory,
    private store: Store<GlobalState>,
    private dialogRef: MatDialogRef<NewDegreeAuditDialogComponent>,
    private snackBar: MatSnackBar,
  ) {
    this.chosenProgram = fb.control('', Validators.required);

    this.chosenAuditSettings = fb.group({
      honorsOptions: fb.control(' ', Validators.required),
      includeCoursesFrom: fb.control('future', Validators.required),
      degreePlan: fb.control('', Validators.required),
    });

    this.chosenCreditSettings = fb.array([]);
  }

  private initDegreeProgram(): Observable<StudentDegreeProgram[]> {
    /**
     * Get the list of degree programs the student is enrolled in. If there are
     * more than one, set the first degree program to be the initial value of
     * the `chosenProgram` form input.
     *
     * This observable will only fire when the dialog is instantiated.
     */
    return this.api.getStudentDegreePrograms().pipe(shareReplay());
  }

  private initDegreePlan(): Observable<DegreePlan[]> {
    /**
     * Get the list of user degree plans so that if they choose to include
     * planned courses in the audit, they can be presented with a choice of
     * which degree plan to load those planned courses from.
     *
     * Also set the primary degree plan to be the initial value of the
     * `degreePlan` form input.
     *
     * This observable will only fire when the dialog is instantiated.
     */
    return this.store.select(degreePlans).pipe(shareReplay());
  }

  private initHonorsOptions(): Observable<HonorsOption[]> {
    interface HasInstitutionCode {
      darsInstitutionCode: string;
    }

    /**
     * Get the honors options based on the parent institution of the degree
     * program chosen in step 1 of the audit dialog. Since different
     * institutions can have different honors options, this subscription needs
     * to be re-run whenever the degree program is switched.
     */
    return combineLatest([
      this.api.getStaticData(),
      this.chosenProgram.valueChanges,
    ]).pipe(
      filter((tuple): tuple is [DegreePrograms, HasInstitutionCode] => {
        return (
          tuple[1] &&
          tuple[1].hasOwnProperty('darsInstitutionCode') &&
          typeof tuple[1].darsInstitutionCode === 'string'
        );
      }),
      map(([degreePrograms, hasInstitutionCode]): [DegreePrograms, string] => {
        return [degreePrograms, hasInstitutionCode.darsInstitutionCode];
      }),
      map(([degreePrograms, darsInstitutionCode]) => {
        if (degreePrograms.hasOwnProperty(darsInstitutionCode)) {
          return degreePrograms[darsInstitutionCode].honorsOptions;
        } else {
          return [];
        }
      }),
    );
  }

  private initVariableCreditCourses(): Observable<CourseWithCreditRange[]> {
    /**
     * When the user chooses a degree plan:
     *
     * 1. Load all of the courses from that degree plan
     * 2. Find all the courses that support a range of credits and that occur
     *    in an active or future term
     * 3. Compute the range of credit values supported by those courses and add
     *    that range to the course objects
     * 4. Populate the `chosenCreditSettings` form input with these courses
     *    using the credit range to populate each course's dropdown.
     */

    // prettier-ignore
    const chosenDegreePlan = (this.chosenAuditSettings.get('degreePlan') as AbstractControl).valueChanges;
    return chosenDegreePlan.pipe(
      filter((maybeDegreePlan): maybeDegreePlan is { roadmapId: number } => {
        return (
          !!maybeDegreePlan &&
          maybeDegreePlan.hasOwnProperty('roadmapId') &&
          typeof maybeDegreePlan['roadmapId'] === 'number'
        );
      }),
      map(maybeDegreePlan => maybeDegreePlan.roadmapId),
      distinctUntilChanged(),
      flatMap(roadmapId => this.api.getAllCourses(roadmapId)),
      map(courses => {
        return courses.filter(course => {
          return (
            !!course.creditMin &&
            !!course.creditMax &&
            course.creditMax > course.creditMin &&
            this.courseIsFromAnActiveOrFutureTerm(course)
          );
        });
      }),
      map(courses => {
        return courses.map(
          (course): CourseWithCreditRange => {
            return {
              ...course,
              range: inclusiveRange(
                course.creditMin || 0,
                course.creditMax || 0,
              ),
            };
          },
        );
      }),
      shareReplay(),
    );
  }

  public ngOnInit() {
    this.degreePrograms$ = this.initDegreeProgram().pipe(
      tap(dps => dps.length > 0 && this.chosenProgram.setValue(dps[0])),
    );

    this.variableCreditCourses$ = this.initVariableCreditCourses().pipe(
      tap(courses => {
        /**
         * This feels hacky and hopefully there's a better way to remove the old
         * course credit inputs and replace them with new course credit inputs.
         */

        // Remove the old credit settings form inputs
        while (this.chosenCreditSettings.length > 0) {
          this.chosenCreditSettings.removeAt(0);
        }

        // Add the new credit settings form inputs
        courses.forEach(course => {
          const initialCreditValue = course.credits || '';
          const creditsDropdown = this.fb.control(
            initialCreditValue,
            Validators.required,
          );
          const newCreditSetting = this.fb.group({ course, creditsDropdown });
          this.chosenCreditSettings.push(newCreditSetting);
        });
      }),
    );

    this.variableCreditCoursesSubscription = this.variableCreditCourses$.subscribe();

    this.degreePlans$ = this.initDegreePlan().pipe(
      tap(plans => {
        const primaryPlan = plans.find(plan => plan.primary);
        this.chosenAuditSettings.controls.degreePlan.setValue(primaryPlan);
      }),
    );

    this.honorsOptions$ = this.initHonorsOptions();
  }

  public ngOnDestroy() {
    this.variableCreditCoursesSubscription.unsubscribe();
  }

  private courseIsFromAnActiveOrFutureTerm(course: CourseBase): boolean {
    if (course.termCode === null || this.termCodeService.isNotInitialized()) {
      return false;
    }

    const termCode = this.termCodeService.fromString(course.termCode);
    return termCode.isActive() || termCode.isFuture();
  }

  public darsInstitutionCode<T>(fallback: T): string | T {
    if (typeof this.chosenProgram.value === 'object') {
      return this.chosenProgram.value.darsInstitutionCode;
    } else {
      return fallback;
    }
  }

  public darsDegreeProgramCode<T>(fallback: T): string | T {
    if (typeof this.chosenProgram.value === 'object') {
      return this.chosenProgram.value.darsDegreeProgramCode;
    } else {
      return fallback;
    }
  }

  public degreePlanId(): number {
    // prettier-ignore
    return (this.chosenAuditSettings.controls.degreePlan.value as DegreePlan).roadmapId;
  }

  public degreePlannerPlanName<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('degreePlan');
    const coursesControl = this.chosenAuditSettings.get('includeCoursesFrom');

    if (coursesControl && coursesControl.value === 'planned') {
      if (control && control.value !== null) {
        return control.value.name;
      } else {
        return fallback;
      }
    } else {
      return fallback;
    }
  }

  public darsHonorsOptionCode<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('honorsOptions');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  // If a degree plan name is included in the request we do not need this property
  public includeCoursesFrom<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('includeCoursesFrom');
    if (control && control.value !== 'planned') {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public fixedCredits(): NewDegreeAuditFields['fixedCredits'] {
    return (this.chosenCreditSettings.value as any[]).map(
      (group: { course: CourseBase; creditsDropdown: number }) => {
        return {
          termCode: group.course.termCode || '',
          subjectCode: group.course.subjectCode || '',
          recordId: group.course.id as number,
          credits: group.creditsDropdown,
        };
      },
    );
  }

  public submitAudit() {
    this.dialogRef.close({
      roadmapId: this.degreePlanId(),
      darsInstitutionCode: this.darsInstitutionCode(''),
      darsDegreeProgramCode: this.darsDegreeProgramCode(''),
      degreePlannerPlanName: this.degreePlannerPlanName(''),
      darsHonorsOptionCode: this.darsHonorsOptionCode(''),
      whichEnrolledCoursesIncluded: this.includeCoursesFrom(''),
      fixedCredits: this.fixedCredits(),
    });
    this.snackBar.open('New Degree Audit has been requested');
  }
}
