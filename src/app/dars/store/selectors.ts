import { GlobalState } from '@app/core/state';
import { createSelector } from '@ngrx/store';
import { DARSState } from './state';

export const getDARSState = ({ dars }: GlobalState) => {
  return dars;
};

export const alerts = createSelector(
  getDARSState,
  (state: DARSState) => state.alerts,
);

export const degreePlans = createSelector(
  getDARSState,
  (state: DARSState) => state.degreePlans,
);

export const metadataStatus = createSelector(
  getDARSState,
  (state: DARSState) => state.metadata.status,
);

export const programMetadata = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return state.metadata.programMetadata;
    } else {
      return {};
    }
  },
);

export const outstandingAndPendingPrograms = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return {
        outstanding: state.metadata.outstanding.program,
        pending: state.metadata.pending.program,
      };
    } else {
      return {
        outstanding: 0,
        pending: 0,
      };
    }
  },
);

export const whatIfMetadata = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return state.metadata.whatIfMetadata;
    } else {
      return {};
    }
  },
);

export const outstandingAndPendingWhatIf = createSelector(
  getDARSState,
  (state: DARSState) => {
    if (state.metadata.status === 'Loaded') {
      return {
        outstanding: state.metadata.outstanding.whatIf,
        pending: state.metadata.pending.whatIf,
      };
    } else {
      return {
        outstanding: 0,
        pending: 0,
      };
    }
  },
);

export const getAudits = createSelector(
  getDARSState,
  (state: DARSState) => state.audits,
);

export const getAudit = createSelector(
  getDARSState,
  (state: DARSState, darsDegreeAuditReportId: number) =>
    state.audits[darsDegreeAuditReportId] || { status: 'NotLoaded' },
);

export const getAuditStatus = createSelector(
  getDARSState,
  (state: DARSState, darsDegreeAuditReportId: string) =>
    (state.audits[darsDegreeAuditReportId] || { status: 'NotLoaded' }).status,
);

export const visibleAudit = createSelector(
  getDARSState,
  (state: DARSState, darsDegreeAuditReportId: string) => {
    const info = state.audits[darsDegreeAuditReportId];
    if (info && info.status === 'Loaded') {
      return info.audit;
    } else {
      return null;
    }
  },
);
