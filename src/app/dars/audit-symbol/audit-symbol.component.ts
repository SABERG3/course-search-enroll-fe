import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AuditSymbol, AuditIconSymbol } from '../models/audit-symbols';

@Component({
  selector: 'cse-audit-symbol',
  templateUrl: './audit-symbol.component.html',
  styleUrls: ['./audit-symbol.component.scss'],
})
export class AuditSymbolComponent {
  @Input() symbol: AuditSymbol;
  @Input() disableTooltip?: boolean;
  @Input() size = 24;

  // Used to typecast because ng-switch is dumb
  asIcon(symbol: AuditSymbol): AuditIconSymbol {
    return symbol as AuditIconSymbol;
  }
}
