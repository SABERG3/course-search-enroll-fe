import { Pipe, PipeTransform } from '@angular/core';
import { AuditMetadata } from '../models/audit-metadata';

@Pipe({ name: 'sortMetadata', pure: true })
export class SortMetadataPipe implements PipeTransform {
  transform(metadata: AuditMetadata[]) {
    return [...metadata].sort((a, b) => {
      if (a.darsAuditRunDate > b.darsAuditRunDate) {
        return -1;
      } else if (a.darsAuditRunDate < b.darsAuditRunDate) {
        return 1;
      } else {
        return 0;
      }
    });
  }
}
