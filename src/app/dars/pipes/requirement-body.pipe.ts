import { Pipe, PipeTransform } from '@angular/core';
import {
  Requirement,
  ContentType,
  SubRequirementCourses,
} from '../models/audit/requirement';

@Pipe({ name: 'requirementBody', pure: true })
export class RequirementBodyPipe implements PipeTransform {
  transform(requirement: Requirement) {
    return requirement.requirementContents.reduce<
      (
        | ContentType & { template: 'lines' }
        | SubRequirementCourses & { template: 'courses' })[]
    >((acc, item) => {
      switch (item.contentType) {
        case 'okRequirementTitle':
        case 'noRequirementTitle':
        case 'hText':
          return acc;
        case 'okSubrequirementCourses':
        case 'noSubrequirementCourses':
          return [...acc, { ...item, template: 'courses' }];
        default:
          return [...acc, { ...item, template: 'lines' }];
      }
    }, []);
  }
}
