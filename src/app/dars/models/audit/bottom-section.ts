export interface AuditBottomSection {
  memorandaSection: null | {
    label: string;
    memorandaLines: string[];
  };
  exceptionsSection: null | {
    label: string;
    exceptions: AuditException[];
  };
}

export interface AuditException {
  date: string;
  exceptionTypeCode: string;
  memo: string;
}
