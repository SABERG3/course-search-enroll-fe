export interface HonorsOption {
  darsInstitutionCode: string;
  darsHonorsOptionCode: string;
  darsHonorsOptionDescription: string;
}
