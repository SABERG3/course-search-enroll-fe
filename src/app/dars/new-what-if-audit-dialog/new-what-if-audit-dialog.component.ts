import { Component, OnInit, OnDestroy } from '@angular/core';
import { DarsApiService } from '../services/api.service';
import { DegreePrograms, DegreeProgram } from '../models/degree-program';
import { Observable, combineLatest, Subject, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { HonorsOption } from '../models/honors-option';
import {
  map,
  flatMap,
  shareReplay,
  distinctUntilChanged,
  tap,
  filter,
} from 'rxjs/operators';
import { CourseBase } from '@app/core/models/course';
import { Store } from '@ngrx/store';
import { GlobalState } from '@app/core/state';
import { degreePlans } from '../store/selectors';
import { DegreePlan } from '@app/core/models/degree-plan';
import { MatDialogRef } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { TermCodeFactory } from '@app/degree-planner/services/termcode.factory';

const inclusiveRange = (from: number, to: number) => {
  const range: number[] = [];
  for (let i = from; i <= to; i++) {
    range.push(i);
  }
  return range;
};

type CourseWithCreditRange = CourseBase & { range: number[] };

export interface NewWhatIfAuditFields {
  roadmapId: number;
  darsInstitutionCode: string;
  darsDegreeProgramCode: string;
  degreePlannerPlanName: string;
  darsHonorsOptionCode: string;
  whichEnrolledCoursesIncluded: string;
  fixedCredits: {
    termCode: string;
    subjectCode: string;
    recordId: number;
    credits: number;
  }[];
}

@Component({
  selector: 'cse-new-what-if-audit-dialog',
  templateUrl: './new-what-if-audit-dialog.component.html',
  styleUrls: ['./new-what-if-audit-dialog.component.scss'],
})
export class NewWhatIfAuditDialogComponent implements OnInit, OnDestroy {
  // Form-builder objects
  public chosenProgram: FormGroup;
  public chosenAuditSettings: FormGroup;
  public chosenCreditSettings: FormArray;

  // API observables
  public institutions$: Observable<DegreePrograms>;
  public programOrPlanOptions$: Observable<DegreeProgram[]>;
  public degreePlans$: Observable<DegreePlan[]>;
  public chosenRoadmapId$ = new Subject<number>();
  public honorsOptions$: Observable<HonorsOption[]>;
  public variableCreditCourses$: Observable<CourseWithCreditRange[]>;
  public variableCreditCoursesSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private api: DarsApiService,
    private termCodeService: TermCodeFactory,
    private store: Store<GlobalState>,
    private dialogRef: MatDialogRef<
      NewWhatIfAuditDialogComponent,
      NewWhatIfAuditFields
    >,
    private snackBar: MatSnackBar,
  ) {
    this.chosenProgram = fb.group({
      institution: fb.control('', Validators.required),
      planOrProgram: fb.control(
        { value: '', disabled: true },
        Validators.required,
      ),
    });

    this.chosenAuditSettings = fb.group({
      honorsOptions: fb.control(' ', Validators.required),
      includeCoursesFrom: fb.control('future', Validators.required),
      degreePlan: fb.control('', Validators.required),
    });

    this.chosenCreditSettings = fb.array([]);
  }

  private initInstitutions() {
    return this.api.getStaticData().pipe(shareReplay());
  }

  private initProgramOrPlanOptions() {
    // prettier-ignore
    const chosenInstitution = this.chosenProgram.controls.institution.valueChanges;

    return combineLatest([this.institutions$, chosenInstitution]).pipe(
      map(([insitutions, darsInstitutionCode]) => {
        if (insitutions.hasOwnProperty(darsInstitutionCode)) {
          return insitutions[darsInstitutionCode].programs;
        } else {
          return [];
        }
      }),
    );
  }

  private initDegreePlans(): Observable<DegreePlan[]> {
    return this.store.select(degreePlans).pipe(shareReplay());
  }

  private initHonorsOptions(): Observable<HonorsOption[]> {
    /**
     * Get the honors options based on the parent insitution of the degree
     * program chosen in step 1 of the audit dialog. Since different
     * insitutions can have different honors options, this subscription needs
     * to be re-run whenever the degree program is switched.
     */

    // prettier-ignore
    const chosenInstitution = this.chosenProgram.controls.institution.valueChanges;

    return combineLatest([this.institutions$, chosenInstitution]).pipe(
      map(([institutions, darsInstitutionCode]) => {
        if (institutions.hasOwnProperty(darsInstitutionCode)) {
          return institutions[darsInstitutionCode].honorsOptions;
        } else {
          return [];
        }
      }),
    );
  }

  private initVariableCreditCourses(): Observable<CourseWithCreditRange[]> {
    /**
     * When the user chooses a degree plan:
     *
     * 1. Load all of the courses from that degree plan
     * 2. Find all the courses that support a range of credits and that occur
     *    in an active or future term
     * 3. Compute the range of credit values supported by those courses and add
     *    that range to the course objects
     * 4. Populate the `chosenCreditSettings` form input with these courses
     *    using the credit range to populate each course's dropdown.
     */

    // prettier-ignore
    const chosenDegreePlan = this.chosenAuditSettings.controls.degreePlan.valueChanges;

    return chosenDegreePlan.pipe(
      filter((maybeDegreePlan): maybeDegreePlan is { roadmapId: number } => {
        return (
          !!maybeDegreePlan &&
          maybeDegreePlan.hasOwnProperty('roadmapId') &&
          typeof maybeDegreePlan['roadmapId'] === 'number'
        );
      }),
      map(maybeDegreePlan => maybeDegreePlan.roadmapId),
      distinctUntilChanged(),
      flatMap(roadmapId => this.api.getAllCourses(roadmapId)),
      map(courses => {
        return courses.filter(course => {
          return (
            !!course.creditMin &&
            !!course.creditMax &&
            course.creditMax > course.creditMin &&
            this.courseIsFromAnActiveOrFutureTerm(course)
          );
        });
      }),
      map(courses => {
        return courses.map(
          (course): CourseWithCreditRange => {
            return {
              ...course,
              range: inclusiveRange(
                course.creditMin || 0,
                course.creditMax || 0,
              ),
            };
          },
        );
      }),
      shareReplay(),
    );
  }

  public ngOnInit() {
    this.institutions$ = this.initInstitutions().pipe(
      tap(() => {
        return this.chosenProgram.controls.planOrProgram.enable();
      }),
    );

    this.programOrPlanOptions$ = this.initProgramOrPlanOptions();

    this.degreePlans$ = this.initDegreePlans().pipe(
      tap(plans => {
        const primaryPlan = plans.find(plan => plan.primary);
        this.chosenAuditSettings.controls.degreePlan.setValue(primaryPlan);
      }),
    );

    this.honorsOptions$ = this.initHonorsOptions();

    this.variableCreditCourses$ = this.initVariableCreditCourses().pipe(
      tap(courses => {
        /**
         * This feels hacky and hopefully there's a better way to remove the old
         * course credit inputs and replace them with new course credit inputs.
         */

        // Remove the old credit settings form inputs
        while (this.chosenCreditSettings.length > 0) {
          this.chosenCreditSettings.removeAt(0);
        }

        // Add the new credit settings form inputs
        courses.forEach(course => {
          const initialCreditValue = course.credits || '';
          const creditsDropdown = this.fb.control(
            initialCreditValue,
            Validators.required,
          );
          const newCreditSetting = this.fb.group({ course, creditsDropdown });
          this.chosenCreditSettings.push(newCreditSetting);
        });
      }),
    );

    this.variableCreditCoursesSubscription = this.variableCreditCourses$.subscribe();
  }

  public ngOnDestroy() {
    this.variableCreditCoursesSubscription.unsubscribe();
  }

  private courseIsFromAnActiveOrFutureTerm(course: CourseBase): boolean {
    if (course.termCode === null || this.termCodeService.isNotInitialized()) {
      return false;
    }

    const termCode = this.termCodeService.fromString(course.termCode);
    return termCode.isActive() || termCode.isFuture();
  }

  public comparePlans(a: DegreePlan | null, b: DegreePlan | null): boolean {
    if (!a || !b) {
      return false;
    } else {
      return a.roadmapId === b.roadmapId;
    }
  }

  public darsInstitutionCode<T>(fallback: T): string | T {
    const control = this.chosenProgram.get('institution');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public darsDegreeProgramCode<T>(fallback: T): string | T {
    const control = this.chosenProgram.get('planOrProgram');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public degreePlanId(): number {
    // prettier-ignore
    return (this.chosenAuditSettings.controls.degreePlan.value as DegreePlan).roadmapId;
  }

  public degreePlannerPlanName<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('degreePlan');
    const coursesControl = this.chosenAuditSettings.get('includeCoursesFrom');

    if (coursesControl && coursesControl.value === 'planned') {
      if (control && control.value !== null) {
        return control.value.name;
      } else {
        return fallback;
      }
    } else {
      return fallback;
    }
  }

  public darsHonorsOptionCode<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('honorsOptions');
    if (control !== null) {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  // If a degree plan name is included in the request we do not need this property
  public includeCoursesFrom<T>(fallback: T): string | T {
    const control = this.chosenAuditSettings.get('includeCoursesFrom');
    if (control && control.value !== 'planned') {
      return control.value.toString();
    } else {
      return fallback;
    }
  }

  public fixedCredits(): NewWhatIfAuditFields['fixedCredits'] {
    return (this.chosenCreditSettings.value as any[]).map(
      (group: { course: CourseBase; creditsDropdown: number }) => {
        return {
          termCode: group.course.termCode || '',
          subjectCode: group.course.subjectCode || '',
          recordId: group.course.id as number,
          credits: group.creditsDropdown,
        };
      },
    );
  }

  public submitAudit() {
    this.dialogRef.close({
      roadmapId: this.degreePlanId(),
      darsInstitutionCode: this.darsInstitutionCode(''),
      darsDegreeProgramCode: this.darsDegreeProgramCode(''),
      degreePlannerPlanName: this.degreePlannerPlanName(''),
      darsHonorsOptionCode: this.darsHonorsOptionCode(''),
      whichEnrolledCoursesIncluded: this.includeCoursesFrom(''),
      fixedCredits: this.fixedCredits(),
    });
    this.snackBar.open("New 'what-if' Audit has been requested");
  }
}
