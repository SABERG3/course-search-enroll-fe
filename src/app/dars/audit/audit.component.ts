import {
  Component,
  Input,
  ViewChild,
  ViewEncapsulation,
  OnInit,
} from '@angular/core';
import { Audit } from '../models/audit/audit';
import {
  Requirement,
  ContentType,
  SubRequirementCourses,
} from '../models/audit/requirement';
import { MatAccordion } from '@angular/material';
import { AuditUnits } from '../models/audit/top-section';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { HostListener } from '@angular/core';
import { AuditSymbolsService } from '../services/audit-symbols.service';

@Component({
  selector: 'cse-dars-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DarsAuditComponent implements OnInit {
  @Input() audit: Audit;
  @ViewChild(MatAccordion) requirements: MatAccordion;
  public metadata: any = {};
  public auditId: number;
  public courseTerms: string[] = [
    'term',
    'course',
    'credits',
    'grade',
    'title',
    'note',
  ];

  public highSchoolUnitsSection = ['lang', 'math', 'science', 'soc stud'];
  public scrolling = false;
  public advancedStandingCredits = [
    'dateValue',
    'typeValue',
    'degreeCreditsValue',
    'courseCreditsValue',
  ];
  constructor(
    private announcer: LiveAnnouncer,
    public symbol: AuditSymbolsService,
  ) {}
  public announce(btn) {
    btn === 'expand'
      ? this.announcer.announce(
          'Expanded All Requirements Sections',
          'assertive',
        )
      : this.announcer.announce(
          'Collapsed All Requirements Sections',
          'assertive',
        );
  }

  // Checking for window scroll events
  @HostListener('window:scroll', ['$event'])
  onScroll() {
    window.pageYOffset > 400
      ? (this.scrolling = true)
      : (this.scrolling = false);
  }

  public ngOnInit() {
    // Filter out LEGEND from audit data
    this.audit.requirements = this.audit.requirements.filter(function(
      requirement,
    ) {
      return requirement.requirementName !== 'LEGEND';
    });
    console.log(this.audit);
  }

  public scrollTop() {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
    this.announcer.announce('Scrolling window to the top', 'assertive');
  }

  public openAllRequirements() {
    this.requirements.openAll();
  }

  public closeAllRequirements() {
    this.requirements.closeAll();
  }

  public trackReq(index: number, req: Requirement) {
    return `${index}-${req.requirementName}`;
  }

  public trackByIndex(index: number, i: any) {
    return index;
  }

  public asCourseBody(reqBody: any) {
    return reqBody as (SubRequirementCourses & { template: 'courses' });
  }

  public asLineBody(reqBody: any) {
    return reqBody as (ContentType & { template: 'lines' });
  }

  public formatHighSchoolUnitData(units: AuditUnits[]) {
    const formatted: object[] = [];
    // Get the max number of rows
    const maxRows = units.reduce((acc, unit) => {
      if (unit.subjectUnits && unit.subjectUnits.length > acc) {
        return unit.subjectUnits.length;
      }
      return acc;
    }, 0);

    for (let i = 0; i < maxRows; i++) {
      formatted[i] = {};
      units.forEach(unit => {
        if (!unit.subjectUnits) {
          formatted[i][unit.unitLabel.toLowerCase()] = '';
          return;
        }
        formatted[i][unit.unitLabel.toLowerCase()] = unit.subjectUnits[i]
          ? `${unit.subjectUnits[i].subject} ${unit.subjectUnits[i].unitsTaken}`
          : '';
        return;
      });
    }
    return formatted;
  }
}
