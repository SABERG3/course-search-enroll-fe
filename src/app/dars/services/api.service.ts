import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { DegreePrograms } from '../models/degree-program';
import { AuditMetadata } from '../models/audit-metadata';
import { StudentDegreeProgram } from '../models/student-degree-program';
import { environment } from './../../../environments/environment';
import { Audit } from '../models/audit/audit';
import { CourseBase } from '@app/core/models/course';
const degreeProgramsResponse: DegreePrograms = require('../../../assets/mock-data/degreeprograms-response.json');

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({ providedIn: 'root' })
export class DarsApiService {
  constructor(private http: HttpClient) {}

  /**
   * Get all degree programs, honors, and institution data.
   *
   */
  public getStaticData(): Observable<DegreePrograms> {
    // Prevents errors locally
    if (environment.production) {
      const url = `${environment.apiDarsData}/degreeprograms.json`;
      return this.http.get<DegreePrograms>(url, HTTP_OPTIONS);
    } else {
      return of(degreeProgramsResponse);
    }
  }

  /**
   *
   * Get courses
   */
  public getAllCourses(roadmapId: number): Observable<CourseBase[]> {
    return this.http.get<CourseBase[]>(
      `${environment.apiPlannerUrl}/degreePlan/${roadmapId}/courses`,
      HTTP_OPTIONS,
    );
  }

  /**
   * Get students degree programs.
   */
  public getStudentDegreePrograms(): Observable<StudentDegreeProgram[]> {
    const url = `${environment.apiDarsUrl}/studentplans`;
    return this.http.get<StudentDegreeProgram[]>(url, HTTP_OPTIONS);
  }

  /**
   * Get audit metadata for all audits a user has.
   */
  public getAudits(): Observable<AuditMetadata[]> {
    const url = `${environment.apiDarsUrl}/auditmetadata`;
    return this.http.get<AuditMetadata[]>(url, HTTP_OPTIONS);
  }

  /**
   * Get a single audit.
   */
  public getAudit(reportId: number): Observable<Audit> {
    const url = `${environment.apiDarsUrl}/auditresults?reportId=${reportId}`;
    return this.http.get<any>(url, HTTP_OPTIONS);
  }

  /**
   * Request a new audit
   */
  public newAudit(params: {
    darsInstitutionCode: string;
    darsDegreeProgramCode: string;
    degreePlannerPlanName?: string;
    whichEnrolledCoursesIncluded?: string;
    darsHonorsOptionCode: string;
  }): Observable<{ darsJobId: string }> {
    const url = `${environment.apiDarsUrl}/auditrequest`;
    return this.http.post<{ darsJobId: string }>(
      url,
      { ...params },
      HTTP_OPTIONS,
    );
  }
}
