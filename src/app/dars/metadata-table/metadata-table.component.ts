import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { AuditMetadata } from '../models/audit-metadata';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, combineLatest, Subscription } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { LiveAnnouncer } from '@angular/cdk/a11y';

interface MinimumAuditMetadata {
  darsAuditRunDate: AuditMetadata['darsAuditRunDate'];
  darsDegreeAuditReportId: AuditMetadata['darsDegreeAuditReportId'];
  darsHonorsOptionDescription: AuditMetadata['darsHonorsOptionDescription'];
  darsInstitutionCodeDescription: AuditMetadata['darsInstitutionCodeDescription'];
  darsStatusOfDegreeAuditRequest: AuditMetadata['darsStatusOfDegreeAuditRequest'];
  degreePlannerPlanName: AuditMetadata['degreePlannerPlanName'];
}

@Component({
  selector: 'cse-dars-metadata-table',
  templateUrl: './metadata-table.component.html',
  styleUrls: ['./metadata-table.component.scss'],
})
export class DarsMetadataTableComponent implements OnInit, OnDestroy {
  @Input() public waiting: Observable<{ outstanding: number; pending: number }>;
  @Input() public metadata: Observable<AuditMetadata[]>;
  @Input() public type: 'program' | 'whatIf';
  @Input() public title: string;
  @Input() public tagline: string;
  @Input() public button: string;

  @Output() public openAudit = new EventEmitter<AuditMetadata>();
  @Output() buttonClick = new EventEmitter();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  public pageEvent: PageEvent;
  public dataSource: MatTableDataSource<MinimumAuditMetadata>;
  public displayedColumns = [
    'school',
    'program',
    'honors',
    'plan',
    'status',
    'actions',
  ];
  private dataSubscription: Subscription;

  public static sortMetadata(metadata: MinimumAuditMetadata[]) {
    return metadata.sort((a, b) => {
      const aDate = new Date(a.darsAuditRunDate);
      const bDate = new Date(b.darsAuditRunDate);

      if (aDate < bDate) {
        return 1;
      }
      if (aDate > bDate) {
        return -1;
      }
      return 0;
    });
  }

  constructor(private announcer: LiveAnnouncer) {}

  onPaginationChange(event) {
    this.announcer.announce(
      `Switched page of the ${this.title} table`,
      'assertive',
    );
  }

  public ngOnInit() {
    this.dataSubscription = combineLatest([
      this.waiting,
      this.metadata,
    ]).subscribe(([waiting, metadata]) => {
      const sorted = DarsMetadataTableComponent.sortMetadata(metadata);

      const totalWaiting = waiting.outstanding + waiting.pending;
      const synthetic = Array.from({ length: totalWaiting }).map(() => ({
        darsAuditRunDate: '',
        darsDegreeAuditReportId: null,
        darsHonorsOptionDescription: '',
        darsInstitutionCodeDescription: '',
        darsStatusOfDegreeAuditRequest: '',
        degreePlannerPlanName: '',
      }));

      this.dataSource = new MatTableDataSource<MinimumAuditMetadata>([
        ...synthetic,
        ...sorted,
      ]);

      this.dataSource.paginator = this.paginator;
    });
  }

  public ngOnDestroy() {
    this.dataSubscription.unsubscribe();
  }
}
