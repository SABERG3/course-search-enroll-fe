import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConstantsService } from './degree-planner/services/constants.service';
import { DegreePlannerViewComponent } from './degree-planner/degree-planner-view/degree-planner-view.component';
import { DARSViewComponent } from './dars/dars-view/dars-view.component';
import { AuditViewComponent } from './dars/dars-audit-view/dars-audit-view.component';

const routes: Routes = [
  {
    path: 'dars/:darsDegreeAuditReportId',
    resolve: { constants: ConstantsService },
    component: AuditViewComponent,
    data: { animation: 'AuditView' },
  },
  {
    path: 'dars',
    resolve: { constants: ConstantsService },
    component: DARSViewComponent,
    data: { animation: 'DarsView' },
  },
  {
    path: '',
    resolve: { constants: ConstantsService },
    component: DegreePlannerViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
