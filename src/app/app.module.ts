import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { A11yModule } from '@angular/cdk/a11y';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from '@app/core/core.module';
import { SharedModule } from '@app/shared/shared.module';
import { HeaderComponent } from './core/header/header.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CourseDetailsDialogComponent } from './degree-planner/dialogs/course-details-dialog/course-details-dialog.component';
import { FeedbackDialogComponent } from './degree-planner/dialogs/feedback-dialog/feedback-dialog.component';
import { CreditOverloadDialogComponent } from './degree-planner/dialogs/credit-overload-dialog/credit-overload-dialog.component';
import { GoogleAnalyticsService } from './shared/services/google-analytics.service';
import { IE11WarningDialogComponent } from './degree-planner/dialogs/ie11-warning-dialog/ie11-warning-dialog.component';
import { DARSModule } from './dars/dars.module';
import { DegreePlannerModule } from './degree-planner/degree-planner.module';
import { WebsocketService } from './shared/services/websocket.service';
import { environment } from '@env/environment';
import { WindowRef } from './dars/services/window.service';

@NgModule({
  imports: [
    DARSModule,
    DegreePlannerModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    MatAutocompleteModule,
    A11yModule,
    NgxLinkifyjsModule.forRoot(),
    StoreDevtoolsModule.instrument({
      maxAge: environment.production ? 10 : undefined,
    }),
  ],
  declarations: [AppComponent, HeaderComponent],
  entryComponents: [
    CourseDetailsDialogComponent,
    FeedbackDialogComponent,
    CreditOverloadDialogComponent,
    IE11WarningDialogComponent,
  ],
  providers: [GoogleAnalyticsService, WebsocketService, WindowRef],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
  constructor(
    protected _googleAnalyticsService: GoogleAnalyticsService,
    protected _websocketService: WebsocketService,
  ) {}
}
