import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedModule } from '@app/shared/shared.module';
import { CoreModule } from '@app/core/core.module';

import { CourseDetailsComponent } from './course-details.component';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CourseDetails } from '@app/core/models/course-details';

describe('CourseDetailsComponent', () => {
  let component: CourseDetailsComponent;
  let fixture: ComponentFixture<CourseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, HttpClientModule],
      providers: [{ provide: MAT_DIALOG_DATA, useValue: {} }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseDetailsComponent);
    component = fixture.componentInstance;
    const courseDetails: CourseDetails = {
      termCode: '1194',
      courseId: '011615',
      subject: {
        termCode: '1194',
        subjectCode: '600',
        description: 'MATHEMATICS',
        shortDescription: 'MATH',
        formalDescription: 'MATHEMATICS',
        undergraduateCatalogURI: 'http://pubs.wisc.edu/ug/ls_math.htm',
        graduateCatalogURI: 'http://grad.wisc.edu/catalog/degrees_math.htm',
        departmentURI: 'https://www.math.wisc.edu/',
        uddsFundingSource: 'A4854',
        schoolCollege: {
          academicOrgCode: 'L',
          academicGroupCode: 'L&S',
          shortDescription: 'Letters and Science',
          formalDescription: 'Letters and Science, College of',
          uddsCode: null,
          schoolCollegeURI: 'http://www.ls.wisc.edu/',
        },
        footnotes: [''],
      },
      catalogNumber: '141',
      approvedForTopics: false,
      topics: [],
      minimumCredits: 3,
      maximumCredits: 3,
      creditRange: '3',
      firstTaught: '0974',
      lastTaught: '1192',
      generalEd: {
        code: 'QR-A',
        description: 'Quantitative Reasoning Part A',
      },
      ethnicStudies: null,
      breadths: [],
      lettersAndScienceCredits: {
        code: 'C',
        description: 'Counts as LAS credit (L&S)',
      },
      workplaceExperience: null,
      foreignLanguage: null,
      honors: null,
      levels: [
        {
          code: 'E',
          description: 'Elementary',
        },
      ],
      openToFirstYear: false,
      advisoryPrerequisites: null,
      enrollmentPrerequisites:
        'MATH 96 or placement into MATH 141. MATH 118 does not fulfill the requisite',
      allCrossListedSubjects: [],
      title: 'Quantitative Reasoning and Problem Solving',
      description: '',
      catalogPrintFlag: false,
      academicGroupCode: null,
      currentlyTaught: true,
      gradingBasis: {
        code: 'OPT',
        description: 'Student Option',
      },
      repeatable: 'N',
      gradCourseWork: false,
      instructorProvidedContent: null,
      courseRequirements: {
        '013562=': [55720],
      },
      courseDesignation: 'MATH 141',
      courseDesignationRaw: 'MATH 141',
      fullCourseDesignation: 'MATHEMATICS 141',
      fullCourseDesignationRaw: 'MATHEMATICS 141',
      lastUpdated: 1543905958133,
      catalogSort: '00141',
      subjectAggregate: 'MATHEMATICS 600',
      titleSuggest: {
        input: ['Quantitative Reasoning and Problem Solving'],
        payload: {
          courseId: '011615',
        },
      },
    };
    component.courseDetails = courseDetails;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
