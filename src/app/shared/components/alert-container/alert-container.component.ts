import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Alert } from '@app/core/models/alert';

@Component({
  selector: 'cse-alert-container',
  templateUrl: './alert-container.component.html',
  styleUrls: ['./alert-container.component.scss'],
})
export class AlertContainerComponent {
  @Input() public alerts: Alert[];
  @Output() public dismiss = new EventEmitter<string>();

  public dismissAlert(key: string, callback?: () => void) {
    this.dismiss.emit(key);
    if (typeof callback === 'function') {
      callback();
    }
  }
}
