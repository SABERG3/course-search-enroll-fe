import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

// Form imports
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { Year } from '@app/core/models/year';

@Component({
  selector: 'cse-prompt-dialog',
  templateUrl: './prompt-dialog.component.html',
  styleUrls: ['../dialogs.scss'],
})
export class PromptDialogComponent implements OnInit {
  title: string;
  text: string[];
  inputName: string;
  placeholder?: string;
  tooltip?: string;
  cancelText: string;
  confirmText: string;
  maxLength: number;
  confirmColor: string;
  public termsByYear$: Observable<Year[]>;

  inputForm: FormGroup;
  initialValue: string;
  constructor(
    private dialogRef: MatDialogRef<PromptDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any,
  ) {
    const {
      title = 'User Input Required',
      text = [],
      inputName = 'Data',
      initialValue = '',
      placeholder,
      tooltip,
      maxLength,
      cancelText = 'Cancel',
      confirmText = 'Confirm',
      confirmColor = 'primary',
    } = data;

    this.title = title;
    this.text = text;
    this.inputName = inputName;
    this.placeholder = placeholder;
    this.tooltip = tooltip;
    this.cancelText = cancelText;
    this.confirmText = confirmText;
    this.maxLength = maxLength;
    this.confirmColor = confirmColor;
    this.initialValue = initialValue;

    if (typeof text === 'string') {
      this.text = [text];
    }
  }

  ngOnInit() {
    // Deafults for input form
    let formValidators = [Validators.required];
    if (this.maxLength) {
      formValidators.push(Validators.maxLength(this.maxLength));
    }

    this.inputForm = new FormGroup({
      value: new FormControl(this.initialValue || '', formValidators),
    });
  }

  confirm() {
    const { value } = this.inputForm.value;
    this.dialogRef.close({ confirmed: true, value });
  }

  cancel() {
    const { value } = this.inputForm.value;
    this.dialogRef.close({ confirmed: false, value });
  }
}
