import { WindowRef } from './../../dars/services/window.service';
import { Injectable, OnInit } from '@angular/core';
import Sockette from 'sockette';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';
import { Observable, Subject } from 'rxjs';

const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

export class TokenPayload {
  token: string;
}
export class ConnectionStatus {
  state: string;
  message: string;
  timestamp: string;
}

@Injectable()
export class WebsocketService implements OnInit {
  ws: Sockette;
  timer: number;
  public connectionState = new Subject<string>();
  public messages = new Subject<string>();

  ngOnInit() {
    this.connectionState.next('Disconnected');
  }

  constructor(private http: HttpClient, private winRef: WindowRef) {}

  connectToServer(token: string) {
    const websocketEndpoint =
      this.winRef.nativeWindow &&
      this.winRef.nativeWindow.config &&
      this.winRef.nativeWindow.config.wsendpoint
        ? this.winRef.nativeWindow.config.wsendpoint
        : ' ';
    const connectURl = `${websocketEndpoint}?token=${token}`;
    console.log('Websocket Connect URL:', connectURl);

    this.ws = new Sockette(connectURl, {
      timeout: 5000,
      maxAttempts: 10,
      onopen: e => {
        console.log('Connected! at ' + new Date().toISOString(), e);
        this.setStatus('Connected!', e);
        // send a ping every 5 minutes to keep the connection alive
        // it seems to have a 10 minute inactivity timeout
        this.timer = window.setInterval(() => {
          this.ws.json({ type: 'ping' });
        }, 1000 * 60 * 5);
      },
      onmessage: e => {
        console.log('Received:', e);
        // this.setStatus('Received:', e.data);
        this.messages.next(e.data);
      },
      onreconnect: e => {
        console.log('Reconnecting...', e);
        this.setStatus('Reconnecting...', e);
      },
      onmaximum: e => {
        console.log('Stop Attempting!', e);
        this.setStatus('Stop Attempting!', e);
      },
      onclose: e => {
        console.log('Closed! at ' + new Date().toISOString(), e);
        this.setStatus('Closed:', e);
        clearInterval(this.timer);
      },
      onerror: e => {
        console.log('Error:', e);
        this.setStatus('Error:', e);
      },
    });
  }
  disconnect() {
    this.ws.close();
  }
  setStatus(message, event) {
    const timestamp = new Date().toTimeString();
    this.connectionState.next(message);
  }
  getToken(): Observable<any> {
    return this.http.post('/wsregister', {}, HTTP_OPTIONS);
  }
}
