import { ConstantsService } from '@app/degree-planner/services/constants.service';
import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { environment } from '../../../environments/environment';
declare var ga: Function; // <-- Here we declare GA variable

@Injectable()
export class GoogleAnalyticsService {
  constructor(router: Router, constants: ConstantsService) {
    if (!environment.production) {
      return;
    }

    router.events.subscribe(event => {
      const studentDataCareer = constants.getStudentInfo()
        ? constants.getStudentInfo().primaryCareer
        : false;
      const careerAndAcademicLevel = studentDataCareer
        ? studentDataCareer.careerCode +
          '-' +
          studentDataCareer.academicLevelDescription
        : 'None';

      if (event instanceof NavigationEnd) {
        ga('set', 'dimension1', careerAndAcademicLevel);
        ga('send', 'pageview', '/degree-planner');
      }
    });
  }
}
