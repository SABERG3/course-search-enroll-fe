import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'academicYearRange' })
export class AcademicYearRangePipe implements PipeTransform {
  transform(yearCode: string, args?: any): any {
    // Get the century digit
    const century = parseInt(yearCode.substring(0, 1), 0) + 19;

    // Get the year
    const academicYear = parseInt(yearCode.substring(1), 0);

    const endYear = century * 100 + academicYear;
    const startYear = endYear - 1;

    const yearString = `${startYear} - ${endYear}`;

    return yearString;
  }
}
