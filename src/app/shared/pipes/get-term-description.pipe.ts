import { Pipe, PipeTransform } from '@angular/core';
import { RawTermCode } from '@app/degree-planner/shared/term-codes/without-era';

@Pipe({ name: 'getTermDescription' })
export class GetTermDescriptionPipe implements PipeTransform {
  transform(termCode: RawTermCode | string): string {
    if (typeof termCode === 'string') {
      termCode = new RawTermCode(termCode);
    }

    return termCode.description;
  }
}
