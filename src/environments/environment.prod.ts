export const environment = {
  production: true,
  version: '1.0.45',
  apiPlannerUrl: '/api/planner/v1',
  apiSearchUrl: '/api/search/v1',
  apiEnrollUrl: '/api/enroll/v1',
  apiDarsUrl: '/api/dars',
  apiDarsData: '/api/data',
  snackbarDuration: 4000,
};
